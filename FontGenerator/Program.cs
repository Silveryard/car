﻿using System;
using System.IO;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*  
 *  File Layout:
 *  byte                  name_len  
 *  byte[name_len]        ascii name
 *  byte                  font_size
 *  byte                  char_count
 *  char_data[char_count] data
 *  
 *  char_data{
 *      byte char
 *      byte width
 *      byte height
 *      byte[width*height*4] data //RGBA
 *  };
 */

 /*
  * Args Layout:
  * [0] font_name
  * [1] font_size
  * [2] output_file
  */

namespace FontGenerator {
    class Program {
        static void Main(string[] args) {
            int fontSize = int.Parse(args[1]);
            Font f = new Font(args[0], fontSize);
            List<CharData> data = GetData(f);

            Write(data, args[0], fontSize, args[2]);
            //TestRead(args[2]);
        }

        static void Write(List<CharData> data, string font, int size, String outFile) {
            using (FileStream fs = new FileStream(outFile, FileMode.Create)) {
                byte[] ascii_name = Encoding.ASCII.GetBytes(font);

                fs.WriteByte((byte)ascii_name.Length);
                fs.Write(ascii_name, 0, ascii_name.Length);
                fs.WriteByte((byte)size);
                fs.WriteByte((byte)data.Count);
                
                foreach(CharData d in data) {
                    fs.WriteByte(d.c);
                    fs.WriteByte(d.width);
                    fs.WriteByte(d.height);
                    fs.Write(d.data, 0, d.data.Length);
                }    
            }
        }

        static void TestRead(String filePath) {
            using (FileStream fs = new FileStream(filePath, FileMode.Open)) {
                byte[] name_length = new byte[1];
                fs.Read(name_length, 0, 1);

                byte[] name = new byte[name_length[0]];
                fs.Read(name, 0, name_length[0]);

                byte[] font_size = new byte[1];
                fs.Read(font_size, 0, 1);

                byte[] char_count = new byte[1];
                fs.Read(char_count, 0, 1);

                for(int i = 0; i < char_count[0]; i++) {
                    byte[] c_c = new byte[1];
                    byte[] c_w = new byte[1];
                    byte[] c_h = new byte[1];

                    fs.Read(c_c, 0, 1);
                    fs.Read(c_w, 0, 1);
                    fs.Read(c_h, 0, 1);

                    int size = c_w[0] * c_h[0];
                    byte[] data = new byte[size * 4];
                    fs.Read(data, 0, size * 4);

                    for(int x = 0; x < c_w[0]; x++) {
                        for(int y = 0; y < c_h[0]; y++) {
                            int index = x * c_h[0] + y;

                            byte r = data[(index * 4) + 0];
                            byte g = data[(index * 4) + 1];
                            byte b = data[(index * 4) + 2];
                            byte a = data[(index * 4) + 3];

                            if(c_c[0] == '!')
                                Console.Write(data[(index * 4) + 3] > 0 ? 'X' : ' ');
                        }
                        if (c_c[0] == '!')
                            Console.WriteLine("");
                    }
                    if (c_c[0] == '!')
                        Console.WriteLine("");
                }
            }
        }

        static List<CharData> GetData(Font f) {
            List<CharData> data = new List<CharData>();

            for(byte i = 32; i < 127; i++) {
                char c = Encoding.ASCII.GetChars(new byte[] { i })[0];
                data.Add(GetCharData(f, c));
            }

            return data;
        }

        static CharData GetCharData(Font f, char c) {
            CharData cData = new CharData();
            cData.c = Encoding.ASCII.GetBytes(new char[] { c })[0];

            Bitmap img = new Bitmap(1, 1);
            Graphics drawing = Graphics.FromImage(img);

            SizeF textSize = drawing.MeasureString("" + c, f);

            img.Dispose();
            drawing.Dispose();

            img = new Bitmap((int)textSize.Width, (int)textSize.Height);
            drawing = Graphics.FromImage(img);

            drawing.Clear(Color.FromArgb(0, 0, 0, 0));
            Brush textBrush = new SolidBrush(Color.FromArgb(255, 255, 255, 255));

            drawing.DrawString("" + c, f, textBrush, 0, 0);
            drawing.Save();

            textBrush.Dispose();
            drawing.Dispose();

            cData.width = (byte)img.Width;
            cData.height = (byte)img.Height;
            cData.data = new byte[cData.width * cData.height * 4];
            
            for (int x = 0; x < cData.width; x++) {
                for(int y = 0; y < cData.height; y++) {
                    int index = (x * cData.height + y) * 4;
                    Color col = img.GetPixel(x, y);

                    cData.data[index + 0] = col.R;
                    cData.data[index + 1] = col.G;
                    cData.data[index + 2] = col.B;
                    cData.data[index + 3] = col.A;
                }
            }

            return cData;
        }
    }
}

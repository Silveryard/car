﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FontGenerator {
    class CharData {
        public byte c;
        public byte width;
        public byte height;
        public byte[] data;
    }
}

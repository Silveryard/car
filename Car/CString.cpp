#include "CString.h"

#include "unistd.h"
#include <vector>
#include <cstring>

using namespace std;

CString::CString(Font* f, int x, int y, char* text, int max_chars)
{
	m_x = x;
	m_y = y;
	m_text = text;
	m_f = f;
	m_max_chars = max_chars;
	
	m_cmd_set = NULL;
	m_cmd_count = 0;

	update();
}

void CString::set_string(char* text, int max_chars) {
	m_text = text;
	m_max_chars = max_chars;
	
	update();
}
void CString::set_pos(int x, int y) {
	m_x = x;
	m_y = y;
	
	update();
}

int CString::get_width() {
	return m_w;
}
int CString::get_height() {
	return m_h;
}
int CString::get_x() {
	return m_x;
}
int CString::get_y() {
	return m_y;
}

void CString::get_commands(drawable_cmd** commands, int* count) {
	*commands = m_cmd_set;
	*count = m_cmd_count;
}

void CString::update() {
	type = strcat("String: ", "");

	if (m_cmd_set != NULL) {
		for (int i = 0; i < m_cmd_count; i++)
			delete m_cmd_set[i].data;
		delete m_cmd_set;
	}
	m_cmd_count = 0;

	int count = 0;
	while (true)
	{
		if (m_max_chars != -1 && count >= m_max_chars)
			break;
		
		if (m_text[count] == '\0')
			break;
		
		if (m_f->get_char(m_text[count]) == NULL)
		{
			m_text[count] = ' ';
		}
		
		count++;
	}
	
	char_data** c_d = new char_data*[count];
	int width = 0;
	int height = 0;
	
	for (int i = 0; i < count; i++)
	{
		char_data* cc_d = m_f->get_char(m_text[i]);
		c_d[i] = cc_d;
	
		if (height < cc_d->h)
			height = cc_d->h;
		
		width += cc_d->w;
	}
	
	m_w = width;
	m_h = height;

	vector<drawable_cmd> cmds;

	int x_offset = 0;
	for (int i = 0; i < count; i++) {
		char_data* cd = m_f->get_char(m_text[i]);
		pixel_data* pd = cd->data;

		for (int x = 0; x < cd->w; x++) {
			for (int y = 0; y < cd->h; y++) {
				int pd_i = x + y * cd->w;
				
				if (pd[pd_i].a == 0)
					continue;

				drawable_cmd n_p;
				if (pd[pd_i].a == 255)
					n_p.type = DRAWABLE_CMD_PIXEL;
				else
					n_p.type = DRAWABLE_CMD_PIXEL_ALPHA;
				n_p.x = m_x + x + x_offset;
				n_p.y = m_y + y;
				n_p.width = 1;
				n_p.height = 1;
				n_p.data = new int[1];
				n_p.data[0] = Screen::get_instance()->get_color(pd[pd_i].r, pd[pd_i].g, pd[pd_i].b, pd[pd_i].a);

				cmds.push_back(n_p);
			}
		}

		x_offset += cd->w;
	}

	m_cmd_set = new drawable_cmd[cmds.size()];

	for (int i = 0; i < cmds.size(); i++)
		m_cmd_set[i] = cmds.at(i);

	m_cmd_count = cmds.size();
	
	delete c_d;
}
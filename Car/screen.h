#ifndef _CAR_SCREEN_H_
#define _CAR_SCREEN_H_

#include <vector>
#include "framebuffer.h"

#define DRAWABLE_CMD_PIXEL 1
#define DRAWABLE_CMD_PIXEL_ALPHA 2
#define DRAWABLE_CMD_ROW 3
#define DRAWABLE_CMD_RECT_ALPHA 4
#define DRAWABLE_CMD_FILL 5

struct drawable_cmd {
	/* Defines how the data should be interpreted */
	char type;
	
	/* Start point */
	int x, y;
	/* Width and height of the drawing area */
	int width, height;
	/* Pixel Data */
	int* data;
};

class Drawable {
public:
	char* type;
	virtual void get_commands(drawable_cmd** commands, int* count) = 0;
};

class Screen
{
private:
	static Screen* s_instance;

public:
	static Screen* get_instance();

private:
	Framebuffer m_f;

	std::vector<Drawable*> m_drawables;
	std::vector<Drawable*> m_drawables_to_add;
	std::vector<Drawable*> m_drawables_to_rem;
	
public:
	Screen();
	~Screen();
	
	void add_drawable(Drawable* d);
	void remove_drawable(Drawable* d);
	
	void redraw();
	
	int get_color(int r, int g, int b, int a);
	void get_rgba(int c, int* r, int* g, int* b, int* a);

	int get_width();
	int get_height();
};

#endif

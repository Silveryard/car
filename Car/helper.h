#ifndef _CAR_HELPER_H_
#define _CAR_HELPER_H_

#include <string>
#include <vector>

std::vector<std::string> split_str(std::string str, char separator);
bool str_ends_with(std::string str, std::string end);

std::string call_system_read_out(std::string cmd);

void list_files(std::vector<std::string>& files, std::string dir, std::vector<std::string>* endings);

#endif
#ifndef _CAR_LIST_H_
#define _CAR_LIST_H_

#include <vector>

#include "framebuffer.h"
#include "rect.h"
#include "CString.h"

class List {
private:
	const int BACKGROUND_TRANSPARENCY = 100;
	const int RGB_SELECTED = 255;
	const int RGB_UNSELECT = 116;

	int m_selected_index;
	int m_selected_background;
	int m_string_offset;
	
	std::vector<Rect*> m_backgrounds;
	std::vector<CString*> m_elements;
	std::vector<Drawable*> m_selection_strings;
	
	bool m_needs_update;
	
public:
	List(int base_x);
	
	void set_elements(std::vector<CString*> strings);
	
	void move_down();
	void move_up();
	
	int get_selected_index();
	
	std::vector<Drawable*> get_drawables();
	bool needs_update();
	
private:
	void update_selection_strings();
};

#endif

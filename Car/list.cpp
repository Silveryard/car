#include "list.h"

using namespace std;

List::List(int base_x) {
	m_selected_index = 0;
	m_string_offset = 0;
	m_selected_background = 0;
	if (m_backgrounds.size() == 0) {
		for (int i = 0; i < 8; i++) {
			Rect* background = new Rect(base_x, 5 + (60 * i), 480, 50, RGB_SELECTED, RGB_SELECTED, RGB_SELECTED, BACKGROUND_TRANSPARENCY);

			if (i != 0)
				background->set_color(RGB_UNSELECT, RGB_UNSELECT, RGB_UNSELECT, BACKGROUND_TRANSPARENCY);
		
			m_backgrounds.push_back(background);
		}
	}
}

void List::set_elements(vector<CString*> strings) {
	m_elements = vector<CString*>(strings);
	
	m_selected_index = 0;
	m_string_offset = 0;
	m_backgrounds[m_selected_background]->set_color(RGB_UNSELECT, RGB_UNSELECT, RGB_UNSELECT, BACKGROUND_TRANSPARENCY);
	m_selected_background = 0;
	m_backgrounds[m_selected_background]->set_color(RGB_SELECTED, RGB_SELECTED, RGB_SELECTED, BACKGROUND_TRANSPARENCY);
	
	update_selection_strings();
}

void List::move_down() {
	if (m_selected_index >= m_elements.size() - 1)
		return;
	
	m_selected_index++;
	
	if (m_selected_background < 7) {
		m_backgrounds.at(m_selected_background)->set_color(RGB_UNSELECT, RGB_UNSELECT, RGB_UNSELECT, BACKGROUND_TRANSPARENCY);
		m_selected_background++;
		m_backgrounds.at(m_selected_background)->set_color(RGB_SELECTED, RGB_SELECTED, RGB_SELECTED, BACKGROUND_TRANSPARENCY);
	}
	else {
		m_string_offset++;
		update_selection_strings();
	}
}
void List::move_up() {
	if (m_selected_index <= 0)
		return;
	
	m_selected_index--;
	
	if (m_selected_background > 0) {
		m_backgrounds.at(m_selected_background)->set_color(RGB_UNSELECT, RGB_UNSELECT, RGB_UNSELECT, BACKGROUND_TRANSPARENCY);
		m_selected_background--;
		m_backgrounds.at(m_selected_background)->set_color(RGB_SELECTED, RGB_SELECTED, RGB_SELECTED, BACKGROUND_TRANSPARENCY);
	}
	else {
		m_string_offset--;
		update_selection_strings();
	}
}

int List::get_selected_index() {
	return m_selected_index;
}

vector<Drawable*> List::get_drawables() {
	vector<Drawable*> m_d;
	
	for (int i = 0; i < m_backgrounds.size(); i++)
		m_d.push_back(m_backgrounds.at(i));
	
	for (int i = 0; i < m_selection_strings.size(); i++)
		m_d.push_back(m_selection_strings.at(i));
	
	return m_d;
}
bool List::needs_update() {
	return m_needs_update;
}

void List::update_selection_strings() {
	m_selection_strings.clear();
	for (int i = m_string_offset; i < m_string_offset + 8; i++) {
		if (i >= m_elements.size())
			break;
		
		CString* d = m_elements.at(i);
		d->set_pos(d->get_x(), 15 + 60 * (i - m_string_offset));
		m_selection_strings.push_back(d);
	}
	
	m_needs_update = true;
}
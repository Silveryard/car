#ifndef _CAR_IMAGE_H_
#define _CAR_IMAGE_H_

#include "screen.h"

class Image : public Drawable
{
public:
	static Image* load(int x, int y, const char* filename);
	
private:
	int m_x, m_y;
	int m_w, m_h;
	int* m_data;

	drawable_cmd* m_cmd_set;
	int m_cmd_count;

public:
	Image(int x, int y, int width, int height, char* buffer);
	
	void set_pos(int x, int y);
	int get_width();
	int get_height();
	
	virtual void get_commands(drawable_cmd** commands, int* count);
	
private:
	void update();
};

#endif

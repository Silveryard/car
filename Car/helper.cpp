#include "helper.h"

#include <unistd.h>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <sys/stat.h>
#include <scsi/scsi.h>
#include <scsi/sg.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <fcntl.h>

#include <string.h>
#include <dirent.h>
#include <limits.h>
#include <sstream>

using namespace std;

vector<string> split_str(string str, char separator) {
	vector<string> res;
	string cur_str("");
	
	for (int i = 0; i < str.size(); i++) {
		char cur = str.at(i);
		
		if (cur == separator) {
			res.push_back(cur_str);
			cur_str = string("");
		}
		else {
			cur_str += cur;
		}
	}
	
	res.push_back(cur_str);
	
	return res;
}
bool str_ends_with(string str, string end) {
	if (str.size() < end.size())
		return false;
	
	return (0 == str.compare(str.length() - end.length(), end.length(), end));
}

string call_system_read_out(string cmd) {
	char buffer[128];
	string result = "";
	FILE* pipe = popen(cmd.c_str(), "r");
	if (!pipe) {
		perror("popen() failed!"); 
		return "";
	}
	try {
		while (!feof(pipe)) {
			if (fgets(buffer, 128, pipe) != NULL)
				result += buffer;
		}
	}
	catch (...) {
		pclose(pipe);
		throw;
	}
	pclose(pipe);
	
	result = result.substr(0, result.size() - 1);
	return result;
}

void list_files(vector<string>& files, string dir, vector<string>* endings) {
	DIR * d;

	    /* Open the directory specified by "dir_name". */

	d = opendir(dir.c_str());

	    /* Check it was opened. */
	if (!d) {
		fprintf(stderr,
			"Cannot open directory '%s': %s\n",
			dir.c_str(),
			strerror(errno));
		exit(EXIT_FAILURE);
	}
	while (1) {
		struct dirent * entry;
		const char * d_name;

		        /* "Readdir" gets subsequent entries from "d". */
		entry = readdir(d);
		if (!entry) {
		    /* There are no more entries in this directory, so break
		       out of the while loop. */
			break;
		}
		d_name = entry->d_name;
		/* Print the name of the file and directory. */
		//printf("%s/%s\n", dir.c_str(), d_name);

		/* If you don't want to print the directories, use the
			following line: */

		if (!(entry->d_type & DT_DIR)) {
			string file_name(d_name);
			
			if (endings == NULL || endings->size() == 0) {
				files.push_back(dir + "/" + file_name);
			}
			else {
				for (int i = 0; i < endings->size(); i++) {
					string str_ending = string(".") + endings->at(i); 
					bool correct_ending = str_ends_with(file_name, str_ending);
			
					if (correct_ending) {
						files.push_back(dir + "/" + file_name);	
						break;
					}
				}
			}
		}



		if (entry->d_type & DT_DIR) {

		            /* Check that the directory is not "d" or d's parent. */
            
			if (strcmp(d_name, "..") != 0 &&
			    strcmp(d_name, ".") != 0) {
				int path_length;
				char path[PATH_MAX];
 
				path_length = snprintf(path,
					PATH_MAX,
					"%s/%s",
					dir.c_str(),
					d_name);
				//printf("%s\n", path);
				if (path_length >= PATH_MAX) {
					fprintf(stderr, "Path length has got too long.\n");
					exit(EXIT_FAILURE);
				}
				/* Recursively call "list_dir" with the new path. */
				list_files(files, path, endings);
			}
		}
	}
	/* After going through all the entries, close the directory. */
	if (closedir(d)) {
		fprintf(stderr,
			"Could not close '%s': %s\n",
			dir.c_str(),
			strerror(errno));
		exit(EXIT_FAILURE);
	}
}
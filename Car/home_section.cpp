#include "section.h"

#include "globals.h"

using namespace std;

HomeSection::HomeSection(int width, int height, int id, UPDATE_METHOD* method, SET_METHOD* setter, Font* serif_16)
	: Section(id, method, setter) {
	m_drawables = vector<Drawable*>();
	m_skoda_logo = Image::load(width / 2 - 150, height / 2 - 150, "/home/pi/car/system/images/skoda_logo.rif");
	m_drawables.push_back(m_skoda_logo);
	m_bt_str = new CString(serif_16, 800 - BUTTON_WIDTH + 55, 45, "BT");
	m_drawables.push_back(m_bt_str);
}

vector<Drawable*>* HomeSection::get_drawables() {
	return &m_drawables; 
}
void HomeSection::update_input(section_input input) {
	if (input.input1) {
		this->set_section(SECTION_BLUETOOTH);
	}	
}
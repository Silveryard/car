#ifndef _CAR_BLUETOOTH_PLAYER_H_
#define _CAR_BLUETOOTH_PLAYER_H_

#include <string>
#include <vector>

enum bluetooth_status {
	BLUETOOTH_STATUS_PAUSED,
	BLUETOOTH_STATUS_PLAYING
};

enum bluetooth_result {
	BLUETOOTH_RESULT_OK,
	BLUETOOTH_RESULT_NOT_CONNECTED
};

class BluetoothPlayer {
private:
	static BluetoothPlayer* s_instance;
	
public:
	typedef void(BLUETOOTH_UPDATED)();
	static BluetoothPlayer* get_instance();
	
private:
	const std::string METHOD_TRACK = "org.bluez.MediaPlayer1.Track";
	const std::string METHOD_POS = "org.bluez.MediaPlayer1.Position";
	const std::string METHOD_STATUS = "org.bluez.MediaPlayer1.Status";
	
	const std::string METHOD_NEXT = "org.bluez.MediaPlayer1.Next";
	const std::string METHOD_PLAY = "org.bluez.MediaPlayer1.Play";
	const std::string METHOD_PAUSE = "org.bluez.MediaPlayer1.Pause";
	
	std::string m_device_mac;
	
	bool m_update_connected;
	char* m_update_album;
	char* m_update_artist;
	char* m_update_title;
	int m_update_duration;
	int m_update_pos;
	bluetooth_status m_update_status;
	std::vector<BLUETOOTH_UPDATED*> m_update_listeners;
	
public:
	bluetooth_result is_connected();
	bluetooth_result get_track_info(char** album, char** artist, char** title, int* duration);
	bluetooth_result get_pos(int* pos);
	bluetooth_result get_status(bluetooth_status* status);
	
	bluetooth_result next();
	bluetooth_result play();
	bluetooth_result pause();
	
	void register_listener(BLUETOOTH_UPDATED* listener);
	void unregister_listener(BLUETOOTH_UPDATED* listener);
	
	void update();
	
private:
	BluetoothPlayer();
	
	std::string call_cmd(std::string method);
};

#endif // !_CAR_BLUETOOTH_PLAYER_H_

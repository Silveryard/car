#include "section.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "globals.h"

using namespace std;
using namespace FMOD;

MusicSection* instance;

void lib_updater() {
	instance->update_music_lib();
}

void print_fmod_error(FMOD_RESULT r) {
	switch (r) {
		case FMOD_ERR_BADCOMMAND:            printf("%s\n", " Tried to call a function on a data type that does not allow this type of functionality (ie calling Sound::lock on a streaming sound). "); break;
		case FMOD_ERR_CHANNEL_ALLOC:         printf("%s\n", " Error trying to allocate a channel. "); break;
		case FMOD_ERR_CHANNEL_STOLEN:        printf("%s\n", " The specified channel has been reused to play another sound. "); break;
		case FMOD_ERR_DMA:                   printf("%s\n", " DMA Failure.  See debug output for more information. "); break;
		case FMOD_ERR_DSP_CONNECTION:        printf("%s\n", " DSP connection error.  Connection possibly caused a cyclic dependency or connected dsps with incompatible buffer counts. "); break;
		case FMOD_ERR_DSP_DONTPROCESS:       printf("%s\n", " DSP return code from a DSP process query callback.  Tells mixer not to call the process callback and therefore not consume CPU.  Use this to optimize the DSP graph. "); break;
		case FMOD_ERR_DSP_FORMAT:            printf("%s\n", " DSP Format error.  A DSP unit may have attempted to connect to this network with the wrong format: or a matrix may have been set with the wrong size if the target unit has a specified channel map. "); break;
		case FMOD_ERR_DSP_INUSE:             printf("%s\n", " DSP is already in the mixer's DSP network. It must be removed before being reinserted or released. "); break;
		case FMOD_ERR_DSP_NOTFOUND:          printf("%s\n", " DSP connection error.  Couldn't find the DSP unit specified. "); break;
		case FMOD_ERR_DSP_RESERVED:          printf("%s\n", " DSP operation error.  Cannot perform operation on this DSP as it is reserved by the system. "); break;
		case FMOD_ERR_DSP_SILENCE:           printf("%s\n", " DSP return code from a DSP process query callback.  Tells mixer silence would be produced from read: so go idle and not consume CPU.  Use this to optimize the DSP graph. "); break;
		case FMOD_ERR_DSP_TYPE:              printf("%s\n", " DSP operation cannot be performed on a DSP of this type. "); break;
		case FMOD_ERR_FILE_BAD:              printf("%s\n", " Error loading file. "); break;
		case FMOD_ERR_FILE_COULDNOTSEEK:     printf("%s\n", " Couldn't perform seek operation.  This is a limitation of the medium (ie netstreams) or the file format. "); break;
		case FMOD_ERR_FILE_DISKEJECTED:      printf("%s\n", " Media was ejected while reading. "); break;
		case FMOD_ERR_FILE_EOF:              printf("%s\n", " End of file unexpectedly reached while trying to read essential data (truncated?). "); break;
		case FMOD_ERR_FILE_ENDOFDATA:        printf("%s\n", " End of current chunk reached while trying to read data. "); break;
		case FMOD_ERR_FILE_NOTFOUND:         printf("%s\n", " File not found. "); break;
		case FMOD_ERR_FORMAT:                printf("%s\n", " Unsupported file or audio format. "); break;
		case FMOD_ERR_HEADER_MISMATCH:       printf("%s\n", " There is a version mismatch between the FMOD header and either the FMOD Studio library or the FMOD Low Level library. "); break;
		case FMOD_ERR_HTTP:                  printf("%s\n", " A HTTP error occurred. This is a catch-all for HTTP errors not listed elsewhere. "); break;
		case FMOD_ERR_HTTP_ACCESS:           printf("%s\n", " The specified resource requires authentication or is forbidden. "); break;
		case FMOD_ERR_HTTP_PROXY_AUTH:       printf("%s\n", " Proxy authentication is required to access the specified resource. "); break;
		case FMOD_ERR_HTTP_SERVER_ERROR:     printf("%s\n", " A HTTP server error occurred. "); break;
		case FMOD_ERR_HTTP_TIMEOUT:          printf("%s\n", " The HTTP request timed out. "); break;
		case FMOD_ERR_INITIALIZATION:        printf("%s\n", " FMOD was not initialized correctly to support this function. "); break;
		case FMOD_ERR_INITIALIZED:           printf("%s\n", " Cannot call this command after System::init. "); break;
		case FMOD_ERR_INTERNAL:              printf("%s\n", " An error occurred that wasn't supposed to.  Contact support. "); break;
		case FMOD_ERR_INVALID_FLOAT:         printf("%s\n", " Value passed in was a NaN: Inf or denormalized float. "); break;
		case FMOD_ERR_INVALID_HANDLE:        printf("%s\n", " An invalid object handle was used. "); break;
		case FMOD_ERR_INVALID_PARAM:         printf("%s\n", " An invalid parameter was passed to this function. "); break;
		case FMOD_ERR_INVALID_POSITION:      printf("%s\n", " An invalid seek position was passed to this function. "); break;
		case FMOD_ERR_INVALID_SPEAKER:       printf("%s\n", " An invalid speaker was passed to this function based on the current speaker mode. "); break;
		case FMOD_ERR_INVALID_SYNCPOINT:     printf("%s\n", " The syncpoint did not come from this sound handle. "); break;
		case FMOD_ERR_INVALID_THREAD:        printf("%s\n", " Tried to call a function on a thread that is not supported. "); break;
		case FMOD_ERR_INVALID_VECTOR:        printf("%s\n", " The vectors passed in are not unit length: or perpendicular. "); break;
		case FMOD_ERR_MAXAUDIBLE:            printf("%s\n", " Reached maximum audible playback count for this sound's soundgroup. "); break;
		case FMOD_ERR_MEMORY:                printf("%s\n", " Not enough memory or resources. "); break;
		case FMOD_ERR_MEMORY_CANTPOINT:      printf("%s\n", " Can't use FMOD_OPENMEMORY_POINT on non PCM source data: or non mp3/xma/adpcm data if FMOD_CREATECOMPRESSEDSAMPLE was used. "); break;
		case FMOD_ERR_NEEDS3D:               printf("%s\n", " Tried to call a command on a 2d sound when the command was meant for 3d sound. "); break;
		case FMOD_ERR_NEEDSHARDWARE:         printf("%s\n", " Tried to use a feature that requires hardware support. "); break;
		case FMOD_ERR_NET_CONNECT:           printf("%s\n", " Couldn't connect to the specified host. "); break;
		case FMOD_ERR_NET_SOCKET_ERROR:      printf("%s\n", " A socket error occurred.  This is a catch-all for socket-related errors not listed elsewhere. "); break;
		case FMOD_ERR_NET_URL:               printf("%s\n", " The specified URL couldn't be resolved. "); break;
		case FMOD_ERR_NET_WOULD_BLOCK:       printf("%s\n", " Operation on a non-blocking socket could not complete immediately. "); break;
		case FMOD_ERR_NOTREADY:              printf("%s\n", " Operation could not be performed because specified sound/DSP connection is not ready. "); break;
		case FMOD_ERR_OUTPUT_ALLOCATED:      printf("%s\n", " Error initializing output device: but more specifically: the output device is already in use and cannot be reused. "); break;
		case FMOD_ERR_OUTPUT_CREATEBUFFER:   printf("%s\n", " Error creating hardware sound buffer. "); break;
		case FMOD_ERR_OUTPUT_DRIVERCALL:     printf("%s\n", " A call to a standard soundcard driver failed: which could possibly mean a bug in the driver or resources were missing or exhausted. "); break;
		case FMOD_ERR_OUTPUT_FORMAT:         printf("%s\n", " Soundcard does not support the specified format. "); break;
		case FMOD_ERR_OUTPUT_INIT:           printf("%s\n", " Error initializing output device. "); break;
		case FMOD_ERR_OUTPUT_NODRIVERS:      printf("%s\n", " The output device has no drivers installed.  If pre-init: FMOD_OUTPUT_NOSOUND is selected as the output mode.  If post-init: the function just fails. "); break;
		case FMOD_ERR_PLUGIN:                printf("%s\n", " An unspecified error has been returned from a plugin. "); break;
		case FMOD_ERR_PLUGIN_MISSING:        printf("%s\n", " A requested output: dsp unit type or codec was not available. "); break;
		case FMOD_ERR_PLUGIN_RESOURCE:       printf("%s\n", " A resource that the plugin requires cannot be found. (ie the DLS file for MIDI playback) "); break;
		case FMOD_ERR_PLUGIN_VERSION:        printf("%s\n", " A plugin was built with an unsupported SDK version. "); break;
		case FMOD_ERR_RECORD:                printf("%s\n", " An error occurred trying to initialize the recording device. "); break;
		case FMOD_ERR_REVERB_CHANNELGROUP:   printf("%s\n", " Reverb properties cannot be set on this channel because a parent channelgroup owns the reverb connection. "); break;
		case FMOD_ERR_REVERB_INSTANCE:       printf("%s\n", " Specified instance in FMOD_REVERB_PROPERTIES couldn't be set. Most likely because it is an invalid instance number or the reverb doesn't exist. "); break;
		case FMOD_ERR_SUBSOUNDS:             printf("%s\n", " The error occurred because the sound referenced contains subsounds when it shouldn't have: or it doesn't contain subsounds when it should have.  The operation may also not be able to be performed on a parent sound. "); break;
		case FMOD_ERR_SUBSOUND_ALLOCATED:    printf("%s\n", " This subsound is already being used by another sound: you cannot have more than one parent to a sound.  Null out the other parent's entry first. "); break;
		case FMOD_ERR_SUBSOUND_CANTMOVE:     printf("%s\n", " Shared subsounds cannot be replaced or moved from their parent stream: such as when the parent stream is an FSB file. "); break;
		case FMOD_ERR_TAGNOTFOUND:           printf("%s\n", " The specified tag could not be found or there are no tags. "); break;
		case FMOD_ERR_TOOMANYCHANNELS:       printf("%s\n", " The sound created exceeds the allowable input channel count.  This can be increased using the 'maxinputchannels' parameter in System::setSoftwareFormat. "); break;
		case FMOD_ERR_TRUNCATED:             printf("%s\n", " The retrieved string is too long to fit in the supplied buffer and has been truncated. "); break;
		case FMOD_ERR_UNIMPLEMENTED:         printf("%s\n", " Something in FMOD hasn't been implemented when it should be! contact support! "); break;
		case FMOD_ERR_UNINITIALIZED:         printf("%s\n", " This command failed because System::init or System::setDriver was not called. "); break;
		case FMOD_ERR_UNSUPPORTED:           printf("%s\n", " A command issued was not supported by this object.  Possibly a plugin without certain callbacks specified. "); break;
		case FMOD_ERR_VERSION:               printf("%s\n", " The version number of this file format is not supported. "); break;
		case FMOD_ERR_EVENT_ALREADY_LOADED:  printf("%s\n", " The specified bank has already been loaded. "); break;
		case FMOD_ERR_EVENT_LIVEUPDATE_BUSY: printf("%s\n", " The live update connection failed due to the game already being connected. "); break;
		case FMOD_ERR_EVENT_LIVEUPDATE_MISMATCH: printf("%s\n", " The live update connection failed due to the game data being out of sync with the tool. "); break;
		case FMOD_ERR_EVENT_LIVEUPDATE_TIMEOUT: printf("%s\n", " The live update connection timed out. "); break;
		case FMOD_ERR_EVENT_NOTFOUND:        printf("%s\n", " The requested event: bus or vca could not be found. "); break;
		case FMOD_ERR_STUDIO_UNINITIALIZED:  printf("%s\n", " The Studio::System object is not yet initialized. "); break;
		case FMOD_ERR_STUDIO_NOT_LOADED:     printf("%s\n", " The specified resource is not loaded: so it can't be unloaded. "); break;
		case FMOD_ERR_INVALID_STRING:        printf("%s\n", " An invalid string was passed to this function. "); break;
		case FMOD_ERR_ALREADY_LOCKED:        printf("%s\n", " The specified resource is already locked. "); break;
		case FMOD_ERR_NOT_LOCKED:            printf("%s\n", " The specified resource is not locked: so it can't be unlocked. "); break;
		case FMOD_ERR_RECORD_DISCONNECTED:   printf("%s\n", " The specified recording driver has been disconnected. "); break;
		case FMOD_ERR_TOOMANYSAMPLES:        printf("%s\n", " The length provided exceeds the allowable limit. "); break; 
		case FMOD_RESULT_FORCEINT:    printf("%s\n", " Makes sure this enum is signed 32bit. "); break;
	}
}

MusicSection::MusicSection(int id, UPDATE_METHOD* method, SET_METHOD* setter, int button_width, int button_height, Font* serif_12) : Section(id, method, setter) {
	instance = this;
	m_cur_song = NULL;
	m_cur_sound = NULL;
	m_channel = 0;
	m_autoplay = false;
	//system("speaker-test -c 4 -D surround40 -t wav");
	//system("amixer set Speaker 100%");
	//system("amixer -c1 scontents");
	
	MusicLib::get_instance()->init();
	MusicLib::get_instance()->register_update_listener(lib_updater);
	FMOD_RESULT result;
	result = System_Create(&m_music_system);
	if (result != FMOD_OK)
	{
		printf("FMOD ERROR\n");
		print_fmod_error(result);
		exit(-1);
	}
	
	/*int num_drivers;
	result = m_music_system->getNumDrivers(&num_drivers);
	if (result != FMOD_OK)
	{
		printf("FMOD ERROR\n");
		print_fmod_error(result);
		exit(-1);
	}
	for (int i = 0; i < num_drivers; i++) {
		char name[50];
		int name_len = 0;
		FMOD_GUID guid = FMOD_GUID();
		int system_rate = 0;
		FMOD_SPEAKERMODE speaker_mode = FMOD_SPEAKERMODE_DEFAULT;
		char* str_speaker_mode;
		int speaker_mode_channels = 0;
		result = m_music_system->getDriverInfo(i, name, name_len, &guid, &system_rate, &speaker_mode, &speaker_mode_channels);
		if (result != FMOD_OK)
		{
			printf("FMOD ERROR\n");
			print_fmod_error(result);
			exit(-1);
		}
		
		switch (speaker_mode) {
		case FMOD_SPEAKERMODE_DEFAULT:			str_speaker_mode = " Default speaker mode based on operating system/output mode.  Windows = control panel setting, Xbox = 5.1, PS3 = 7.1 etc. "; break;
		case FMOD_SPEAKERMODE_RAW:				str_speaker_mode = " There is no specific speakermode.  Sound channels are mapped in order of input to output.  Use System::setSoftwareFormat to specify speaker count. See remarks for more information. "; break;
		case FMOD_SPEAKERMODE_MONO:				str_speaker_mode = " The speakers are monaural. "; break;
		case FMOD_SPEAKERMODE_STEREO:			str_speaker_mode = " The speakers are stereo. "; break;
		case FMOD_SPEAKERMODE_QUAD:				str_speaker_mode = " 4 speaker setup.    This includes front left, front right, surround left, surround right.  "; break;
		case FMOD_SPEAKERMODE_SURROUND:			str_speaker_mode = " 5 speaker setup.    This includes front left, front right, center, surround left, surround right. "; break;
		case FMOD_SPEAKERMODE_5POINT1:			str_speaker_mode = " 5.1 speaker setup.  This includes front left, front right, center, surround left, surround right and an LFE speaker. "; break;
		case FMOD_SPEAKERMODE_7POINT1:			str_speaker_mode = " 7.1 speaker setup.  This includes front left, front right, center, surround left, surround right, back left, back right and an LFE speaker. "; break;
			
		case FMOD_SPEAKERMODE_MAX:				str_speaker_mode = " Maximum number of speaker modes supported. "; break;
		case FMOD_SPEAKERMODE_FORCEINT:			str_speaker_mode = " Makes sure this enum is signed 32bit. "; break;
		}
		
		char n_name[name_len];
		for (int j = 0; j < name_len; j++) {
			n_name[j] = name[j];
		}
		
		printf("%s-%s-%d\n", n_name, str_speaker_mode, speaker_mode_channels);
	}	 
	
    result = m_music_system->setSoftwareFormat(48000, FMOD_SPEAKERMODE_7POINT1, 5);
	if (result != FMOD_OK)
	{
		printf("FMOD ERROR\n");
		print_fmod_error(result);
		exit(-1);
	} */
	result = m_music_system->init(16, FMOD_INIT_NORMAL, 0);
	if (result != FMOD_OK)
	{
		printf("FMOD ERROR\n");
		print_fmod_error(result);
		exit(-1);
	}
	
	m_serif_12 = serif_12;
	m_button_width = button_width;
	m_button_height = button_height;
	
	m_list = new List(m_button_width + 10);
	
	btn_down = Image::load(690, button_height * 3 + 25, "/home/pi/car/system/images/arrow_down.rif");
	btn_up = Image::load(690, button_height * 2 + 25, "/home/pi/car/system/images/arrow_up.rif");
	btn_back = Image::load(685, button_height * 1 + 25, "/home/pi/car/system/images/back.rif");
	btn_play = Image::load(690, button_height * 0 + 25, "/home/pi/car/system/images/play.rif");
	btn_pause = Image::load(690, button_height * 0 + 25, "/home/pi/car/system/images/pause.rif");
	btn_select = Image::load(685, button_height * 0 + 25, "/home/pi/car/system/images/select.rif");
	btn_next = Image::load(690, button_height * 3 + 25, "/home/pi/car/system/images/next.rif");
	
	m_home_elements.push_back(new CString(m_serif_12, get_string_x(), get_string_y(0), "All Titles"));
	m_home_elements.push_back(new CString(m_serif_12, get_string_x(), get_string_y(1), "Artists"));
	
	m_cur_alb = new CString(m_serif_12, 10, 10, "");
	m_cur_art = new CString(m_serif_12, 10, 10, "");
	m_cur_tit = new CString(m_serif_12, 10, 10, "");
	m_cur_song_hor = new Line(button_width + 30, 350, 800 - (2 * (button_width + 30)), true, 255, 255, 255, 255);
	m_cur_song_ver = new Line(button_width + 30, 330, 40, false, 255, 255, 255, 255);
	
	update_music_lib(false);
}

vector<Drawable*>* MusicSection::get_drawables() {
	m_drawables.clear();
	
	switch (m_cur_mode) {
	case MODE_HOME: {}
	case MODE_ARTISTS:{}
	case MODE_ALBUMS: {									  
		m_drawables.push_back(btn_down);
		m_drawables.push_back(btn_up);
		m_drawables.push_back(btn_back);
		m_drawables.push_back(btn_select);
		break;
	}
	case MODE_ALL_TITLES: {}
	case MODE_ALBUM: {									  
		m_drawables.push_back(btn_down);
		m_drawables.push_back(btn_up);
		m_drawables.push_back(btn_back);
		m_drawables.push_back(btn_play);
		break;
	}
	case MODE_CUR_TRACK: {
		m_drawables.push_back(btn_back);
		m_drawables.push_back(btn_next);
		
		bool paused;
		m_channel->getPaused(&paused);
		
		if (paused)
			m_drawables.push_back(btn_play);
		else
			m_drawables.push_back(btn_pause);
		break;
	}
	}
	
	if (m_cur_mode != MODE_CUR_TRACK) {
		vector<Drawable*> list_drawables = m_list->get_drawables();
	
		for (int i = 0; i < list_drawables.size(); i++)
			m_drawables.push_back(list_drawables.at(i));
	}
	else {
		m_drawables.push_back(m_cur_alb);
		m_drawables.push_back(m_cur_art);
		m_drawables.push_back(m_cur_tit);
		m_drawables.push_back(m_cur_song_hor);
		m_drawables.push_back(m_cur_song_ver);
	}
	
	return &m_drawables;
}
void MusicSection::update_input(section_input input) {
	//Scrolling
	if (input.input3) {
		m_list->move_up();
	}
	
	if (input.input4) {
		m_list->move_down();
	}
	
	switch (m_cur_mode) {
	case MODE_HOME: {
			if (input.input1) {
				if (m_list->get_selected_index() == 0)
					set_mode(MODE_ALL_TITLES);
			
				if (m_list->get_selected_index() == 1)
					set_mode(MODE_ARTISTS);
			}
		
			if (input.input2 && m_cur_song != NULL) {
				set_mode(MODE_CUR_TRACK);
				/*m_cur_sound->release();
				m_cur_sound = NULL;
				m_cur_song = NULL;
				m_autoplay = false;
				m_channel->stop(); */
			}
			break;
		}
		
	case MODE_ALL_TITLES: {
			if (input.input1) {
				m_cur_song = MusicLib::get_instance()->get_all_songs().at(m_list->get_selected_index());
				Sound* sound;
				m_music_system->createStream(m_cur_song->file, FMOD_DEFAULT, NULL, &sound);
			
				if (m_cur_song != NULL) {
					m_cur_sound->release();
				}
			
				m_cur_sound = sound;
				m_channel->setPaused(false);
				m_music_system->playSound(m_cur_sound, NULL, false, &m_channel);
				m_channel->setVolume(1);
				m_autoplay_list = MusicLib::get_instance()->get_all_songs();
				m_autoplay = true;
				set_mode(MODE_CUR_TRACK);
			}	
			
			if (input.input2) 
				set_mode(MODE_HOME);
		break;
		}
		
	case MODE_ARTISTS: {
			if (input.input1) {
				m_selected_artist = MusicLib::get_instance()->get_all_artists().at(m_list->get_selected_index());
				set_mode(MODE_ALBUMS);
			}
		
			if (input.input2)
				set_mode(MODE_HOME);
		break;
		}
	case MODE_ALBUMS: {
			if (input.input1) {
				m_selected_album = m_selected_artist->albums.at(m_list->get_selected_index());
				set_mode(MODE_ALBUM);
			}
		
			if (input.input2)
				set_mode(MODE_ARTISTS);
		break;
		}
	case MODE_ALBUM: {
			if (input.input1) {
				m_cur_song = m_selected_album->songs.at(m_list->get_selected_index());
				Sound* sound;
				m_music_system->createStream(m_cur_song->file, FMOD_DEFAULT, NULL, &sound);
			
				if (m_cur_song != NULL) {
					m_cur_sound->release();
				}
			
				m_cur_sound = sound;
				m_channel->setPaused(false);
				m_music_system->playSound(m_cur_sound, NULL, false, &m_channel);
				m_channel->setVolume(1);
				m_autoplay_list = m_selected_album->songs;
				m_autoplay = true;
				set_mode(MODE_CUR_TRACK);
			}	
			
			if (input.input2) 
				set_mode(MODE_ALBUMS);
		break;
		}
	case MODE_CUR_TRACK: {
		if (input.input1) {
			bool paused;
			m_channel->getPaused(&paused);
			m_channel->setPaused(!paused);
			this->update();
		}
		
		if(input.input2)
			set_mode(MODE_HOME);
		
		if (input.input4)
			next_song();
	}
	}

	if (m_list->needs_update())
		this->update();
}

void MusicSection::update_music_system() {
	m_music_system->update();
	
	if (m_cur_song == NULL)
		return;
	
	if (m_cur_mode == MODE_CUR_TRACK) {
		unsigned int length = 0;
		unsigned int pos = 0;
	
		m_cur_sound->getLength(&length, FMOD_TIMEUNIT_MS);
		m_channel->getPosition(&pos, FMOD_TIMEUNIT_MS);
	
		int min_x = m_cur_song_hor->get_x();
		int max_x = m_cur_song_hor->get_x() + m_cur_song_hor->get_len();

		int x = min_x + ((max_x - min_x) * (pos / (float)length));
		m_cur_song_ver->update_pos(x, m_cur_song_ver->get_y());
	}
	
	bool channel_playing;
	FMOD_RESULT r = m_channel->isPlaying(&channel_playing);
	
	if (!channel_playing && m_autoplay && GLOBALS_MUSIC_AUTOPLAY) {
		next_song();
	}
}

void MusicSection::update_music_lib(bool trigger) {
	set_mode(MODE_HOME, trigger);
	
	//Delete old stuf
	for (map<Artist*, CString*>::iterator it = m_artist_names.begin(); it != m_artist_names.end(); it++) {
		delete it->second;
	}
	for (map<Album*, CString*>::iterator it = m_album_names.begin(); it != m_album_names.end(); it++) {
		delete it->second;
	}
	for (map<Song*, CString*>::iterator it = m_song_names.begin(); it != m_song_names.end(); it++) {
		delete it->second;
	}
	
	m_artist_names.clear();
	m_album_names.clear();
	m_song_names.clear();
	
	//Generate new stuff	
	vector<Artist*> all_artists = MusicLib::get_instance()->get_all_artists();
	
	for (int i = 0; i < all_artists.size(); i++) {
		Artist* artist = all_artists.at(i);
		CString* artist_name = new CString(m_serif_12, get_string_x(), get_string_y(i), artist->name, 35);
		m_artist_names.insert(pair<Artist*, CString*>(artist, artist_name));
		
		for (int j = 0; j < artist->albums.size(); j++) {
			Album* album = artist->albums.at(j);
			CString* album_name = new CString(m_serif_12, get_string_x(), get_string_y(j), album->name, 35);
			m_album_names.insert(pair<Album*, CString*>(album, album_name));
			
			for (int k = 0; k < album->songs.size(); k++) {
				Song* song = album->songs.at(k);
				CString* song_name = new CString(m_serif_12, get_string_x(), get_string_y(k), song->title, 35);
				m_song_names.insert(pair<Song*, CString*>(song, song_name));
			}
		}
	}
}

void MusicSection::set_mode(MODE mode, bool trigger) {
	m_cur_mode = mode;
	
	vector<CString*> elements;
	switch (m_cur_mode) {
	case MODE_HOME: {
		for (int i = 0; i < m_home_elements.size(); i++) {
			elements.push_back(m_home_elements.at(i));
		}
		break;
		}
	case MODE_ALL_TITLES: {
		vector<Song*> all_songs = MusicLib::get_instance()->get_all_songs();
		for (int i = 0; i < all_songs.size(); i++) {
			elements.push_back(m_song_names.find(all_songs.at(i))->second);
		}
		break;
		}
	case MODE_ARTISTS: {
		m_selected_artist = NULL;
		vector<Artist*> all_artists = MusicLib::get_instance()->get_all_artists();
		for (int i = 0; i < all_artists.size(); i++) {
			elements.push_back(m_artist_names.find(all_artists.at(i))->second);
		}
		break;
		}
	case MODE_ALBUMS: {
		m_selected_album = NULL;
		for (int i = 0; i < m_selected_artist->albums.size(); i++) {
			elements.push_back(m_album_names.find(m_selected_artist->albums.at(i))->second);
		}
		break;
		}
	case MODE_ALBUM: {
		for (int i = 0; i < m_selected_album->songs.size(); i++) {
			elements.push_back(m_song_names.find(m_selected_album->songs.at(i))->second);
		}
		break;
		}
	case MODE_CUR_TRACK: {
		m_cur_alb->set_string(m_cur_song->album->name);
		m_cur_art->set_string(m_cur_song->artist->name);
		m_cur_tit->set_string(m_cur_song->title);
																		  
		m_cur_tit->set_pos(400 - (m_cur_tit->get_width() / 2.0f), 100);
		m_cur_alb->set_pos(400 - (m_cur_alb->get_width() / 2.0f), 150);
		m_cur_art->set_pos(400 - (m_cur_art->get_width() / 2.0f), 200);
		break;
	}
	}
	
	m_list->set_elements(elements);
	
	if (trigger && m_list->needs_update())
		this->update();
}
int MusicSection::get_string_x() {
	return m_button_width + 30;
}
int MusicSection::get_string_y(int index) {
	return 20 + (60 * index);
}

void MusicSection::next_song() {
	m_channel->stop();
	m_cur_sound->release();
	
	if (GLOBALS_MUSIC_SHUFFLE)
		m_cur_song = m_autoplay_list.at(rand() % m_autoplay_list.size());
	else {
		int index = -1;
		for (int i = 0; i < m_autoplay_list.size(); i++) {
			if (m_autoplay_list.at(i) == m_cur_song) {
				index = i;
				break;
			}
		}
			
		index++;
		if (index >= m_autoplay_list.size())
			index = 0;
			
		m_cur_song = m_autoplay_list.at(index);
	}
	
	m_music_system->createStream(m_cur_song->file, FMOD_DEFAULT, NULL, &m_cur_sound);
	m_music_system->playSound(m_cur_sound,
		NULL,
		false,
		&
		m_channel);
		
	if (m_cur_mode == MODE_CUR_TRACK)
		set_mode(MODE_CUR_TRACK);
}
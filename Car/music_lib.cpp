#include "music_lib.h"

#include <iostream>
#include <dirent.h>
#include <string>
#include <sys/stat.h>
#include <errno.h>

#include <stdio.h>
#include <unistd.h>

#include <dirent.h>
#include <limits.h>

#include <map>

#include "taglib/fileref.h"	   
#include "helper.h"

using namespace std;
using namespace TagLib;

void mounted_listener(usb_drive* drive) {
	MusicLib::get_instance()->add_usb_lib(drive);
}
void unmounted_listener(usb_drive* drive) {
	MusicLib::get_instance()->remove_usb_drive(drive);
}

bool title_comparer(Song* s1, Song* s2) {
	return string(s1->title).compare(string(s2->title)) < 0;
}
bool artist_comparer(Artist* a1, Artist* a2) {
	return string(a1->name).compare(string(a2->name)) < 0;
}
bool album_comparer(Album* a1, Album* a2) {
	return string(a1->name).compare(string(a2->name)) < 0;
}
bool track_comparer(Song* s1, Song* s2) {
	int c = s1->track < s2->track;
	
	if (c != 0)
		return c;
	
	return string(s1->title).compare(string(s2->title)) < 0;
}

int find_artist(vector<Artist*> artists, string name) {
	for (int i = 0; i < artists.size(); i++) {
		if (name.compare(string(artists.at(i)->name)) == 0)
			return i;
	}
	
	return -1;
}
int find_album(vector<Album*> albums, string name) {
	for (int i = 0; i < albums.size(); i++) {
		if (name.compare(string(albums.at(i)->name)) == 0)
			return i;
	}
	
	return -1;
}

MusicLib* MusicLib::s_instance;

MusicLib* MusicLib::get_instance()
{
	if (s_instance == NULL)
		s_instance = new MusicLib();
	
	return s_instance;
}

void MusicLib::init()
{
	UsbManager::get_instance()->register_mounted_listener(mounted_listener);
	UsbManager::get_instance()->register_unmounted_listener(unmounted_listener);
	
	vector<usb_drive*>* drives = UsbManager::get_instance()->get_mounted_drives();
	for (int i = 0; i < drives->size(); i++) {
		add_usb_lib(drives->at(i));
	}
}

vector<Song*> MusicLib::get_all_songs()
{
	return m_songs;
}
vector<Artist*> MusicLib::get_all_artists() {
	return m_artists;
}

void MusicLib::register_update_listener(LIB_UPDATED* listener) {
	m_update_listeners.push_back(listener);
}
void MusicLib::unregister_update_listener(LIB_UPDATED* listener) {
	for (vector<LIB_UPDATED*>::iterator it = m_update_listeners.begin(); it < m_update_listeners.end(); it++) {
		if (*it == listener) {
			m_update_listeners.erase(it);
			return;
		}
	}
}

void MusicLib::add_usb_lib(usb_drive* drive) {
	vector<string> files;
	vector<string> endings;
	endings.push_back("mp3");
	list_files(files, string(drive->mount_path), &endings);
	vector<Artist*> new_artists = vector<Artist*>();
	
	map<Tag*, string> data;
	
	for (int i = 0; i < files.size(); i++)
	{
		string s_str = files.at(i);
		
		FileRef f_ref(s_str.c_str());
		Tag* t = f_ref.tag();
		data.insert(pair<Tag*, string>(t, s_str));
		
		int artist_index = find_artist(new_artists, string(t->artist().toCString()));
		if (artist_index == -1) {
			Artist* n_a = new Artist();
			n_a->name = strdup(t->artist().toCString());
			new_artists.push_back(n_a);
			artist_index = new_artists.size() - 1;
		}
		
		int album_index = find_album(new_artists.at(artist_index)->albums, string(t->album().toCString()));
		if (album_index == -1) {
			Album* n_a = new Album();
			n_a->artist = new_artists.at(artist_index);
			n_a->name = strdup(t->album().toCString());
			new_artists.at(artist_index)->albums.push_back(n_a);
			album_index = new_artists.at(artist_index)->albums.size() - 1;
		}
		
		Song* s = new Song();
		s->file = strdup(files.at(i).c_str());
		s->title = strdup(t->title().toCString());
		s->artist = new_artists.at(artist_index);
		s->album = new_artists.at(artist_index)->albums.at(album_index);
		s->track = t->track();
		new_artists.at(artist_index)->albums.at(album_index)->songs.push_back(s);
	} 
	
	
	for (int i = 0; i < new_artists.size(); i++) {
		Artist* a = new_artists.at(i);
		
		std::sort(a->albums.begin(), a->albums.end(), album_comparer);
		
		for (int j = 0; j < a->albums.size(); j++) {
			Album* al = a->albums.at(j);
			
			std::sort(al->songs.begin(), al->songs.end(), track_comparer);
		}
	}
	
	UsbList* ul = new UsbList();
	ul->drive = drive;
	ul->artists = new_artists;
	m_usb.push_back(ul);
	
	update_song_list();
}
void MusicLib::remove_usb_drive(usb_drive* drive) {
	for (vector<UsbList*>::iterator it = m_usb.begin(); it < m_usb.end(); it++) {
		if ((*it)->drive == drive) {
			m_usb.erase(it);
			break;
		}
	}
	
	update_song_list();
}

void MusicLib::update_song_list() {
	m_songs.clear();
	m_artists.clear();
	
	for (int u = 0; u < m_usb.size(); u++) {
		for (int a = 0; a < m_usb.at(u)->artists.size(); a++) {
			m_artists.push_back(m_usb.at(u)->artists.at(a));
			
			for (int l = 0; l < m_usb.at(u)->artists.at(a)->albums.size(); l++) {
				for (int t = 0; t < m_usb.at(u)->artists.at(a)->albums.at(l)->songs.size(); t++) {
					m_songs.push_back(m_usb.at(u)->artists.at(a)->albums.at(l)->songs.at(t));
				}
			}
		}
	}
	
	std::sort(m_songs.begin(), m_songs.end(), title_comparer);
	std::sort(m_artists.begin(), m_artists.end(), artist_comparer);
	
	for (int i = 0; i < m_update_listeners.size(); i++) {
		(*m_update_listeners.at(i))();
	}
}
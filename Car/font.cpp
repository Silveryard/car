#include "font.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>

Font* Font::load(char* filename)
{
	int fd;
	
	if ((fd = open(filename, O_RDONLY)) == -1)
	{
		return NULL;
	}
	
	char* c_name_len = new char[1];
	read(fd, c_name_len, 1);
	
	char* c_name = new char[c_name_len[0]];
	read(fd, c_name, c_name_len[0]);
	
	char* c_font_size = new char[1];
	read(fd, c_font_size, 1);
	
	char* c_char_count = new char[1];
	read(fd, c_char_count, 1);
	
	char_data** data = new char_data*[c_char_count[0]];
	for (int i = 0; i < c_char_count[0]; i++)
		data[i] = new char_data();

	for (int i = 0; i < c_char_count[0]; i++) {
		char* c_c = new char[1];
		char* c_w = new char[1];
		char* c_h = new char[1];
		read(fd, c_c, 1);
		read(fd, c_w, 1);
		read(fd, c_h, 1);
		
		data[i]->c = c_c[0];
		data[i]->w = c_w[0];
		data[i]->h = c_h[0];
		int size = data[i]->w * data[i]->h;
		data[i]->data = new pixel_data[size];
		char* buffer = new char[data[i]->w * data[i]->h * 4];
		read(fd, buffer, data[i]->w * data[i]->h * 4);
		
		for (int x = 0; x < data[i]->w; x++)
		{
			for (int y = 0; y < data[i]->h; y++)
			{
				int b_index = x * data[i]->h + y;
				int d_index = x + y * data[i]->w;

				data[i]->data[d_index].r = buffer[(b_index * 4) + 0];
				data[i]->data[d_index].g = buffer[(b_index * 4) + 1];
				data[i]->data[d_index].b = buffer[(b_index * 4) + 2];
				data[i]->data[d_index].a = buffer[(b_index * 4) + 3];
			}
		}
		
		delete c_c;
		delete c_w;
		delete c_h;
		delete buffer;
	}
	
	Font* f = new Font(c_name, c_font_size[0], c_char_count[0], data);
	
	delete c_name_len;
	delete c_font_size;
	delete c_char_count;
	
	return f;
}

Font::Font(char* name, char size, char char_count, char_data** data)
{
	m_name = name;
	m_size = size;
	m_char_count = char_count;
	m_data = data;
}

char* Font::get_name()
{
	return m_name;
}
char Font::get_size()
{
	return m_size;
}

char_data* Font::get_char(char c)
{
	for (int i = 0; i < m_char_count; i++)
	{
		char_data* d = m_data[i];
		char dc = d->c;
		
		if (dc == c)
			return d;
	}
	
	return NULL;
}

void Font::print()
{
	printf("Font Data:\n");
	printf("Name: %s\n", m_name);
	printf("Size: %d\n", m_size);
	printf("\n");
	
	for (int i = 0; i < m_char_count; i++)
	{
		char_data* cd = m_data[i];
		printf("Char: %d \n", (int)cd->c);
		printf("Width: %d \n", (int)cd->w);
		printf("Height: %d \n", (int)cd->h);
		
		for (int x = 0; x < cd->w; x++)
		{
			for (int y = 0; y < cd->h; y++)
			{
				int index = x * cd->h + y;
				printf("[%d, %d, %d, %d]", cd->data[index].r, cd->data[index].g, cd->data[index].b, cd->data[index].a);
			}
			
			printf("\n");
		}
		
		printf("\n");
	}
}
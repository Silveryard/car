#ifndef _CAR_GLOBALS_H_
#define _CAR_GLOBALS_H_

#define SECTION_HOME 0
#define SECTION_RADIO 1
#define SECTION_MUSIC 2
#define SECTION_SETTINGS 3
#define SECTION_BLUETOOTH 4

#define BUTTON_WIDTH 150
#define BUTTON_HEIGHT 120

extern bool GLOBALS_MUSIC_AUTOPLAY;
extern bool GLOBALS_MUSIC_SHUFFLE;

//Value between 0 and 100
extern int GLOBALS_SOUND_VOLUME;
/*
	Defines the balance between left and right speakers
	Value between 0 and 100
	0: All to left
	50: even
	100: All to right
*/
extern int GLOBALS_SOUND_BALANCE;
/*
	Defines the fading between front and back
	Value between 0 and 100
	0: All to front
	50: even
	100: All to back
*/
extern int GLOBALS_SOUND_FADE;

#endif

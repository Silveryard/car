#include "touch.h"

#include <iostream>
#include <unistd.h>
#include <stdio.h>
#include <string>
#include <fcntl.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <linux/input.h>
#include <linux/ioctl.h>

#include <stdlib.h>
#include <scsi/scsi.h>
#include <scsi/sg.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <vector>

#include <dirent.h>
#include <limits.h>
#include <sstream>

#define KWHT  "\x1B[37m"
#define KYEL  "\x1B[33m"

using namespace std;

//#define TOUCH_LOG

Touch::Touch()
{
	init_events();
	init_keys();
	init_absval();
	init_relatives();
	init_absolutes();
	init_misc();
	init_leds();
	init_repeats();
	init_sounds();
	init_names();	 
	
	//Open Screen		   FT5406 memory based driver"
	vector<char*> event_files; 
	DIR * d;

    /* Open the directory specified by "dir_name". */

	d = opendir("/dev/input");

	    /* Check it was opened. */
	if (!d) {
		fprintf(stderr,
			"Cannot open directory '%s': %s\n",
			"/dev/input",
			strerror(errno));
		exit(EXIT_FAILURE);
	}
	while (1) {
		struct dirent * entry;
		const char * d_name;

		        /* "Readdir" gets subsequent entries from "d". */
		entry = readdir(d);
		if (!entry) {
		    /* There are no more entries in this directory, so break
		       out of the while loop. */
			break;
		}
		d_name = entry->d_name;
		
		if (!(entry->d_type & DT_DIR)) {
			if (strlen(d_name) == 6 && (string(d_name).find("event")) == 0) {
				char* c = strdup((string("/dev/input/") + string(d_name)).c_str());
				event_files.push_back(c);
			}
		}
	}
	/* After going through all the entries, close the directory. */
	if (closedir(d)) {
		fprintf(stderr,
			"Could not close '%s': %s\n",
			"/dev/input",
			strerror(errno));
		exit(EXIT_FAILURE);
	}
	
	for (int i = 0; i < event_files.size(); i++) {
		//m_fd = open("/dev/input/event1", O_RDONLY);
		m_fd = open(event_files.at(i), O_RDONLY);
		
		if (init_screen())
			break;
		
		close(m_fd);
	}
}

void Touch::get_sample(int* x_pos, int* y_pos, int* type)
{
	int i, rb;
	struct input_event ev[64];
	
	*type = TOUCH_HOLD;
	
	rb = read(m_fd, ev, sizeof(struct input_event) * 64);
	
	for (i = 0; i <  (rb / sizeof(struct input_event)); i++) {
		if (ev[i].type ==  EV_SYN)
		{
#ifdef TOUCH_LOG
			printf("Event type is %s%s%s = Start of New Event\n", KYEL, m_events[ev[i].type], KWHT);	
#endif
		}
		else if (ev[i].type == EV_KEY && ev[i].code == 330 && ev[i].value == 1)
		{	
#ifdef TOUCH_LOG
			printf("Event type is %s%s%s & Event code is %sTOUCH(330)%s & Event value is %s1%s = Touch Starting\n", KYEL, m_events[ev[i].type], KWHT, KYEL, KWHT, KYEL, KWHT);
#endif
			*type = TOUCH_START;	
		}
		else if (ev[i].type == EV_KEY && ev[i].code == 330 && ev[i].value == 0)
		{
#ifdef TOUCH_LOG
			printf("Event type is %s%s%s & Event code is %sTOUCH(330)%s & Event value is %s0%s = Touch Finished\n", KYEL, m_events[ev[i].type], KWHT, KYEL, KWHT, KYEL, KWHT);
#endif
			*type = TOUCH_END;	
		}
		else if (ev[i].type == EV_ABS && ev[i].code == 0 && ev[i].value > 0) {
#ifdef TOUCH_LOG
			printf("Event type is %s%s%s & Event code is %sX(0)%s & Event value is %s%d%s\n", KYEL, m_events[ev[i].type], KWHT, KYEL, KWHT, KYEL, ev[i].value, KWHT);
#endif
			*x_pos = ev[i].value;
		}
		else if (ev[i].type == EV_ABS  && ev[i].code == 1 && ev[i].value > 0) {
#ifdef TOUCH_LOG
			printf("Event type is %s%s%s & Event code is %sY(1)%s & Event value is %s%d%s\n", KYEL, m_events[ev[i].type], KWHT, KYEL, KWHT, KYEL, ev[i].value, KWHT);
#endif
			*y_pos = ev[i].value;
		}
		else if (ev[i].type == EV_ABS  && ev[i].code == 24 && ev[i].value > 0) {
#ifdef TOUCH_LOG
			printf("Event type is %s%s%s & Event code is %sPressure(24)%s & Event value is %s%d%s\n", KYEL, m_events[ev[i].type], KWHT, KYEL, KWHT, KYEL, ev[i].value, KWHT);
#endif			
			//*rawPressure = ev[i].value;
		}
	}
}

void Touch::init_events()
{
	m_events = new char * [100];
	for (int i = 0; i < EV_MAX; i++)
	{
		m_events[i] = NULL;
	}
	m_events[EV_SYN] = "Sync";
	m_events[EV_KEY] = "Key";
	m_events[EV_REL] = "Relative";
	m_events[EV_ABS] = "Absolute";
	m_events[EV_MSC] = "Misc";
	m_events[EV_LED] = "LED";
	m_events[EV_SND] = "Sound"; 
	m_events[EV_REP] = "Repeat";
	m_events[EV_FF] = "ForceFeedback";
	m_events[EV_PWR] = "Power";
	m_events[EV_FF_STATUS] = "ForceFeedbackStatus";
}
void Touch::init_keys()
{
	m_keys = new char * [1000];
	for (int i = 0; i < KEY_MAX; i++)
	{
		m_keys[i] = NULL;
	}
	m_keys[KEY_RESERVED] = "Reserved";		
	m_keys[KEY_ESC] = "Esc";
	m_keys[KEY_1] = "1";				
	m_keys[KEY_2] = "2";
	m_keys[KEY_3] = "3";				
	m_keys[KEY_4] = "4";
	m_keys[KEY_5] = "5";				
	m_keys[KEY_6] = "6";
	m_keys[KEY_7] = "7";				
	m_keys[KEY_8] = "8";
	m_keys[KEY_9] = "9";				
	m_keys[KEY_0] = "0";
	m_keys[KEY_MINUS] = "Minus";			
	m_keys[KEY_EQUAL] = "Equal";
	m_keys[KEY_BACKSPACE] = "Backspace";		
	m_keys[KEY_TAB] = "Tab";
	m_keys[KEY_Q] = "Q";				
	m_keys[KEY_W] = "W";
	m_keys[KEY_E] = "E";				
	m_keys[KEY_R] = "R";
	m_keys[KEY_T] = "T";				
	m_keys[KEY_Y] = "Y";
	m_keys[KEY_U] = "U";				
	m_keys[KEY_I] = "I";
	m_keys[KEY_O] = "O";				
	m_keys[KEY_P] = "P";
	m_keys[KEY_LEFTBRACE] = "LeftBrace";		
	m_keys[KEY_RIGHTBRACE] = "RightBrace";
	m_keys[KEY_ENTER] = "Enter";			
	m_keys[KEY_LEFTCTRL] = "LeftControl";
	m_keys[KEY_A] = "A";				
	m_keys[KEY_S] = "S";
	m_keys[KEY_D] = "D";				
	m_keys[KEY_F] = "F";
	m_keys[KEY_G] = "G";				
	m_keys[KEY_H] = "H";
	m_keys[KEY_J] = "J";				
	m_keys[KEY_K] = "K";
	m_keys[KEY_L] = "L";				
	m_keys[KEY_SEMICOLON] = "Semicolon";
	m_keys[KEY_APOSTROPHE] = "Apostrophe";	
	m_keys[KEY_GRAVE] = "Grave";
	m_keys[KEY_LEFTSHIFT] = "LeftShift";		
	m_keys[KEY_BACKSLASH] = "BackSlash";
	m_keys[KEY_Z] = "Z";				
	m_keys[KEY_X] = "X";
	m_keys[KEY_C] = "C";				
	m_keys[KEY_V] = "V";
	m_keys[KEY_B] = "B";				
	m_keys[KEY_N] = "N";
	m_keys[KEY_M] = "M";				
	m_keys[KEY_COMMA] = "Comma";
	m_keys[KEY_DOT] = "Dot";			
	m_keys[KEY_SLASH] = "Slash";
	m_keys[KEY_RIGHTSHIFT] = "RightShift";	
	m_keys[KEY_KPASTERISK] = "KPAsterisk";
	m_keys[KEY_LEFTALT] = "LeftAlt";		
	m_keys[KEY_SPACE] = "Space";
	m_keys[KEY_CAPSLOCK] = "CapsLock";		
	m_keys[KEY_F1] = "F1";
	m_keys[KEY_F2] = "F2";			
	m_keys[KEY_F3] = "F3";
	m_keys[KEY_F4] = "F4";			
	m_keys[KEY_F5] = "F5";
	m_keys[KEY_F6] = "F6";			
	m_keys[KEY_F7] = "F7";
	m_keys[KEY_F8] = "F8";			
	m_keys[KEY_F9] = "F9";
	m_keys[KEY_F10] = "F10";			
	m_keys[KEY_NUMLOCK] = "NumLock";
	m_keys[KEY_SCROLLLOCK] = "ScrollLock";	
	m_keys[KEY_KP7] = "KP7";
	m_keys[KEY_KP8] = "KP8";			
	m_keys[KEY_KP9] = "KP9";
	m_keys[KEY_KPMINUS] = "KPMinus";		
	m_keys[KEY_KP4] = "KP4";
	m_keys[KEY_KP5] = "KP5";			
	m_keys[KEY_KP6] = "KP6";
	m_keys[KEY_KPPLUS] = "KPPlus";		
	m_keys[KEY_KP1] = "KP1";
	m_keys[KEY_KP2] = "KP2";			
	m_keys[KEY_KP3] = "KP3";
	m_keys[KEY_KP0] = "KP0";			
	m_keys[KEY_KPDOT] = "KPDot";
	m_keys[KEY_ZENKAKUHANKAKU] = "Zenkaku/Hankaku"; 
	m_keys[KEY_102ND] = "102nd";
	m_keys[KEY_F11] = "F11";			
	m_keys[KEY_F12] = "F12";
	m_keys[KEY_RO] = "RO";			
	m_keys[KEY_KATAKANA] = "Katakana";
	m_keys[KEY_HIRAGANA] = "HIRAGANA";		
	m_keys[KEY_HENKAN] = "Henkan";
	m_keys[KEY_KATAKANAHIRAGANA] = "Katakana/Hiragana"; 
	m_keys[KEY_MUHENKAN] = "Muhenkan";
	m_keys[KEY_KPJPCOMMA] = "KPJpComma";		
	m_keys[KEY_KPENTER] = "KPEnter";
	m_keys[KEY_RIGHTCTRL] = "RightCtrl";		
	m_keys[KEY_KPSLASH] = "KPSlash";
	m_keys[KEY_SYSRQ] = "SysRq";			
	m_keys[KEY_RIGHTALT] = "RightAlt";
	m_keys[KEY_LINEFEED] = "LineFeed";		
	m_keys[KEY_HOME] = "Home";
	m_keys[KEY_UP] = "Up";			
	m_keys[KEY_PAGEUP] = "PageUp";
	m_keys[KEY_LEFT] = "Left";			
	m_keys[KEY_RIGHT] = "Right";
	m_keys[KEY_END] = "End";			
	m_keys[KEY_DOWN] = "Down";
	m_keys[KEY_PAGEDOWN] = "PageDown";		
	m_keys[KEY_INSERT] = "Insert";
	m_keys[KEY_DELETE] = "Delete";		
	m_keys[KEY_MACRO] = "Macro";
	m_keys[KEY_MUTE] = "Mute";			
	m_keys[KEY_VOLUMEDOWN] = "VolumeDown";
	m_keys[KEY_VOLUMEUP] = "VolumeUp";		
	m_keys[KEY_POWER] = "Power";
	m_keys[KEY_KPEQUAL] = "KPEqual";		
	m_keys[KEY_KPPLUSMINUS] = "KPPlusMinus";
	m_keys[KEY_PAUSE] = "Pause";			
	m_keys[KEY_KPCOMMA] = "KPComma";
	m_keys[KEY_HANGUEL] = "Hanguel";		
	m_keys[KEY_HANJA] = "Hanja";
	m_keys[KEY_YEN] = "Yen"; 
	m_keys[KEY_LEFTMETA] = "LeftMeta";
	m_keys[KEY_RIGHTMETA] = "RightMeta"; 
	m_keys[KEY_COMPOSE] = "Compose";
	m_keys[KEY_STOP] = "Stop";			
	m_keys[KEY_AGAIN] = "Again";
	m_keys[KEY_PROPS] = "Props";			
	m_keys[KEY_UNDO] = "Undo";
	m_keys[KEY_FRONT] = "Front";			
	m_keys[KEY_COPY] = "Copy";
	m_keys[KEY_OPEN] = "Open";			
	m_keys[KEY_PASTE] = "Paste";
	m_keys[KEY_FIND] = "Find";			
	m_keys[KEY_CUT] = "Cut";
	m_keys[KEY_HELP] = "Help";			
	m_keys[KEY_MENU] = "Menu";
	m_keys[KEY_CALC] = "Calc";			
	m_keys[KEY_SETUP] = "Setup";
	m_keys[KEY_SLEEP] = "Sleep";			
	m_keys[KEY_WAKEUP] = "WakeUp";
	m_keys[KEY_FILE] = "File";			
	m_keys[KEY_SENDFILE] = "SendFile";
	m_keys[KEY_DELETEFILE] = "DeleteFile";	
	m_keys[KEY_XFER] = "X-fer";
	m_keys[KEY_PROG1] = "Prog1";			
	m_keys[KEY_PROG2] = "Prog2";
	m_keys[KEY_WWW] = "WWW";			
	m_keys[KEY_MSDOS] = "MSDOS";
	m_keys[KEY_COFFEE] = "Coffee";		
	m_keys[KEY_DIRECTION] = "Direction";
	m_keys[KEY_CYCLEWINDOWS] = "CycleWindows";	
	m_keys[KEY_MAIL] = "Mail";
	m_keys[KEY_BOOKMARKS] = "Bookmarks";		
	m_keys[KEY_COMPUTER] = "Computer";
	m_keys[KEY_BACK] = "Back";			
	m_keys[KEY_FORWARD] = "Forward";
	m_keys[KEY_CLOSECD] = "CloseCD";		
	m_keys[KEY_EJECTCD] = "EjectCD";
	m_keys[KEY_EJECTCLOSECD] = "EjectCloseCD";	
	m_keys[KEY_NEXTSONG] = "NextSong";
	m_keys[KEY_PLAYPAUSE] = "PlayPause";		
	m_keys[KEY_PREVIOUSSONG] = "PreviousSong";
	m_keys[KEY_STOPCD] = "StopCD";		
	m_keys[KEY_RECORD] = "Record";
	m_keys[KEY_REWIND] = "Rewind";		
	m_keys[KEY_PHONE] = "Phone";
	m_keys[KEY_ISO] = "ISOKey";			
	m_keys[KEY_CONFIG] = "Config";
	m_keys[KEY_HOMEPAGE] = "HomePage";		
	m_keys[KEY_REFRESH] = "Refresh";
	m_keys[KEY_EXIT] = "Exit";			
	m_keys[KEY_MOVE] = "Move";
	m_keys[KEY_EDIT] = "Edit";			
	m_keys[KEY_SCROLLUP] = "ScrollUp";
	m_keys[KEY_SCROLLDOWN] = "ScrollDown"; 
	m_keys[KEY_KPLEFTPAREN] = "KPLeftParenthesis";
	m_keys[KEY_KPRIGHTPAREN] = "KPRightParenthesis"; 
	m_keys[KEY_F13] = "F13";
	m_keys[KEY_F14] = "F14";			
	m_keys[KEY_F15] = "F15";
	m_keys[KEY_F16] = "F16";			
	m_keys[KEY_F17] = "F17";
	m_keys[KEY_F18] = "F18";			
	m_keys[KEY_F19] = "F19";
	m_keys[KEY_F20] = "F20";			
	m_keys[KEY_F21] = "F21";
	m_keys[KEY_F22] = "F22";			
	m_keys[KEY_F23] = "F23";
	m_keys[KEY_F24] = "F24";			
	m_keys[KEY_PLAYCD] = "PlayCD";
	m_keys[KEY_PAUSECD] = "PauseCD";		
	m_keys[KEY_PROG3] = "Prog3";
	m_keys[KEY_PROG4] = "Prog4";			
	m_keys[KEY_SUSPEND] = "Suspend";
	m_keys[KEY_CLOSE] = "Close";			
	m_keys[KEY_PLAY] = "Play";
	m_keys[KEY_FASTFORWARD] = "Fast Forward";	
	m_keys[KEY_BASSBOOST] = "Bass Boost";
	m_keys[KEY_PRINT] = "Print";			
	m_keys[KEY_HP] = "HP";
	m_keys[KEY_CAMERA] = "Camera";		
	m_keys[KEY_SOUND] = "Sound";
	m_keys[KEY_QUESTION] = "Question";		
	m_keys[KEY_EMAIL] = "Email";
	m_keys[KEY_CHAT] = "Chat";			
	m_keys[KEY_SEARCH] = "Search";
	m_keys[KEY_CONNECT] = "Connect";		
	m_keys[KEY_FINANCE] = "Finance";
	m_keys[KEY_SPORT] = "Sport";			
	m_keys[KEY_SHOP] = "Shop";
	m_keys[KEY_ALTERASE] = "Alternate Erase";	
	m_keys[KEY_CANCEL] = "Cancel";
	m_keys[KEY_BRIGHTNESSDOWN] = "Brightness down"; 
	m_keys[KEY_BRIGHTNESSUP] = "Brightness up";
	m_keys[KEY_MEDIA] = "Media";			
	m_keys[KEY_UNKNOWN] = "Unknown";
	m_keys[BTN_0] = "Btn0";			
	m_keys[BTN_1] = "Btn1";
	m_keys[BTN_2] = "Btn2";			
	m_keys[BTN_3] = "Btn3";
	m_keys[BTN_4] = "Btn4";			
	m_keys[BTN_5] = "Btn5";
	m_keys[BTN_6] = "Btn6";			
	m_keys[BTN_7] = "Btn7";
	m_keys[BTN_8] = "Btn8";			
	m_keys[BTN_9] = "Btn9";
	m_keys[BTN_LEFT] = "LeftBtn";			
	m_keys[BTN_RIGHT] = "RightBtn";
	m_keys[BTN_MIDDLE] = "MiddleBtn";		
	m_keys[BTN_SIDE] = "SideBtn";
	m_keys[BTN_EXTRA] = "ExtraBtn";		
	m_keys[BTN_FORWARD] = "ForwardBtn";
	m_keys[BTN_BACK] = "BackBtn";			
	m_keys[BTN_TASK] = "TaskBtn";
	m_keys[BTN_TRIGGER] = "Trigger";		
	m_keys[BTN_THUMB] = "ThumbBtn";
	m_keys[BTN_THUMB2] = "ThumbBtn2";		
	m_keys[BTN_TOP] = "TopBtn";
	m_keys[BTN_TOP2] = "TopBtn2";			
	m_keys[BTN_PINKIE] = "PinkieBtn";
	m_keys[BTN_BASE] = "BaseBtn";			
	m_keys[BTN_BASE2] = "BaseBtn2";
	m_keys[BTN_BASE3] = "BaseBtn3";		
	m_keys[BTN_BASE4] = "BaseBtn4";
	m_keys[BTN_BASE5] = "BaseBtn5";		
	m_keys[BTN_BASE6] = "BaseBtn6";
	m_keys[BTN_DEAD] = "BtnDead";			
	m_keys[BTN_A] = "BtnA";
	m_keys[BTN_B] = "BtnB";			
	m_keys[BTN_C] = "BtnC";
	m_keys[BTN_X] = "BtnX";			
	m_keys[BTN_Y] = "BtnY";
	m_keys[BTN_Z] = "BtnZ";			
	m_keys[BTN_TL] = "BtnTL";
	m_keys[BTN_TR] = "BtnTR";			
	m_keys[BTN_TL2] = "BtnTL2";
	m_keys[BTN_TR2] = "BtnTR2";			
	m_keys[BTN_SELECT] = "BtnSelect";
	m_keys[BTN_START] = "BtnStart";		
	m_keys[BTN_MODE] = "BtnMode";
	m_keys[BTN_THUMBL] = "BtnThumbL";		
	m_keys[BTN_THUMBR] = "BtnThumbR";
	m_keys[BTN_TOOL_PEN] = "ToolPen";		
	m_keys[BTN_TOOL_RUBBER] = "ToolRubber";
	m_keys[BTN_TOOL_BRUSH] = "ToolBrush";		
	m_keys[BTN_TOOL_PENCIL] = "ToolPencil";
	m_keys[BTN_TOOL_AIRBRUSH] = "ToolAirbrush";	
	m_keys[BTN_TOOL_FINGER] = "ToolFinger";
	m_keys[BTN_TOOL_MOUSE] = "ToolMouse";		
	m_keys[BTN_TOOL_LENS] = "ToolLens";
	m_keys[BTN_TOUCH] = "Touch";			
	m_keys[BTN_STYLUS] = "Stylus";
	m_keys[BTN_STYLUS2] = "Stylus2";		
	m_keys[BTN_TOOL_DOUBLETAP] = "Tool Doubletap";
	m_keys[BTN_TOOL_TRIPLETAP] = "Tool Tripletap"; 
	m_keys[BTN_TOOL_QUADTAP] = "Tool Quadtap";
	m_keys[BTN_GEAR_DOWN] = "WheelBtn";
	m_keys[BTN_GEAR_UP] = "Gear up";		
	m_keys[KEY_OK] = "Ok";
	m_keys[KEY_SELECT] = "Select";		
	m_keys[KEY_GOTO] = "Goto";
	m_keys[KEY_CLEAR] = "Clear";			
	m_keys[KEY_POWER2] = "Power2";
	m_keys[KEY_OPTION] = "Option";		
	m_keys[KEY_INFO] = "Info";
	m_keys[KEY_TIME] = "Time";			
	m_keys[KEY_VENDOR] = "Vendor";
	m_keys[KEY_ARCHIVE] = "Archive";		
	m_keys[KEY_PROGRAM] = "Program";
	m_keys[KEY_CHANNEL] = "Channel";		
	m_keys[KEY_FAVORITES] = "Favorites";
	m_keys[KEY_EPG] = "EPG";			
	m_keys[KEY_PVR] = "PVR";
	m_keys[KEY_MHP] = "MHP";			
	m_keys[KEY_LANGUAGE] = "Language";
	m_keys[KEY_TITLE] = "Title";			
	m_keys[KEY_SUBTITLE] = "Subtitle";
	m_keys[KEY_ANGLE] = "Angle";			
	m_keys[KEY_ZOOM] = "Zoom";
	m_keys[KEY_MODE] = "Mode";			
	m_keys[KEY_KEYBOARD] = "Keyboard";
	m_keys[KEY_SCREEN] = "Screen";		
	m_keys[KEY_PC] = "PC";
	m_keys[KEY_TV] = "TV";			
	m_keys[KEY_TV2] = "TV2";
	m_keys[KEY_VCR] = "VCR";			
	m_keys[KEY_VCR2] = "VCR2";
	m_keys[KEY_SAT] = "Sat";			
	m_keys[KEY_SAT2] = "Sat2";
	m_keys[KEY_CD] = "CD";			
	m_keys[KEY_TAPE] = "Tape";
	m_keys[KEY_RADIO] = "Radio";			
	m_keys[KEY_TUNER] = "Tuner";
	m_keys[KEY_PLAYER] = "Player";		
	m_keys[KEY_TEXT] = "Text";
	m_keys[KEY_DVD] = "DVD";			
	m_keys[KEY_AUX] = "Aux";
	m_keys[KEY_MP3] = "MP3";			
	m_keys[KEY_AUDIO] = "Audio";
	m_keys[KEY_VIDEO] = "Video";			
	m_keys[KEY_DIRECTORY] = "Directory";
	m_keys[KEY_LIST] = "List";			
	m_keys[KEY_MEMO] = "Memo";
	m_keys[KEY_CALENDAR] = "Calendar";		
	m_keys[KEY_RED] = "Red";
	m_keys[KEY_GREEN] = "Green";			
	m_keys[KEY_YELLOW] = "Yellow";
	m_keys[KEY_BLUE] = "Blue";			
	m_keys[KEY_CHANNELUP] = "ChannelUp";
	m_keys[KEY_CHANNELDOWN] = "ChannelDown";	
	m_keys[KEY_FIRST] = "First";
	m_keys[KEY_LAST] = "Last";			
	m_keys[KEY_AB] = "AB";
	m_keys[KEY_NEXT] = "Next";			
	m_keys[KEY_RESTART] = "Restart";
	m_keys[KEY_SLOW] = "Slow";			
	m_keys[KEY_SHUFFLE] = "Shuffle";
	m_keys[KEY_BREAK] = "Break";			
	m_keys[KEY_PREVIOUS] = "Previous";
	m_keys[KEY_DIGITS] = "Digits";		
	m_keys[KEY_TEEN] = "TEEN";
	m_keys[KEY_TWEN] = "TWEN";			
	m_keys[KEY_DEL_EOL] = "Delete EOL";
	m_keys[KEY_DEL_EOS] = "Delete EOS";		
	m_keys[KEY_INS_LINE] = "Insert line";
	m_keys[KEY_DEL_LINE] = "Delete line";
	m_keys[KEY_VIDEOPHONE] = "Videophone";	
	m_keys[KEY_GAMES] = "Games";
	m_keys[KEY_ZOOMIN] = "Zoom In";		
	m_keys[KEY_ZOOMOUT] = "Zoom Out";
	m_keys[KEY_ZOOMRESET] = "Zoom Reset";		
	m_keys[KEY_WORDPROCESSOR] = "Word Processor";
	m_keys[KEY_EDITOR] = "Editor";		
	m_keys[KEY_SPREADSHEET] = "Spreadsheet";
	m_keys[KEY_GRAPHICSEDITOR] = "Graphics Editor"; 
	m_keys[KEY_PRESENTATION] = "Presentation";
	m_keys[KEY_DATABASE] = "Database";		
	m_keys[KEY_NEWS] = "News";
	m_keys[KEY_VOICEMAIL] = "Voicemail";		
	m_keys[KEY_ADDRESSBOOK] = "Addressbook";
	m_keys[KEY_MESSENGER] = "Messenger";		
	m_keys[KEY_DISPLAYTOGGLE] = "Display Toggle";
	m_keys[KEY_SPELLCHECK] = "Spellcheck";	
	m_keys[KEY_LOGOFF] = "Log Off";
	m_keys[KEY_DOLLAR] = "Dollar";		
	m_keys[KEY_EURO] = "Euro";
	m_keys[KEY_FRAMEBACK] = "Frame Back";	 
	m_keys[KEY_FRAMEFORWARD] = "Frame Forward";
	m_keys[KEY_CONTEXT_MENU] = "Context Menu";	
	m_keys[KEY_MEDIA_REPEAT] = "Media Repeat";
	m_keys[KEY_DEL_EOL] = "Delete EOL";		
	m_keys[KEY_DEL_EOS] = "Delete EOS";
	m_keys[KEY_INS_LINE] = "Insert Line";	 
	m_keys[KEY_DEL_LINE] = "Delete Line";
	m_keys[KEY_FN] = "Fn";			
	m_keys[KEY_FN_ESC] = "Fn Esc";
	m_keys[KEY_FN_F1] = "Fn F1";			
	m_keys[KEY_FN_F2] = "Fn F2";
	m_keys[KEY_FN_F3] = "Fn F3";			
	m_keys[KEY_FN_F4] = "Fn F4";
	m_keys[KEY_FN_F5] = "Fn F5";			
	m_keys[KEY_FN_F6] = "Fn F6";
	m_keys[KEY_FN_F7] = "Fn F7";			
	m_keys[KEY_FN_F8] = "Fn F8";
	m_keys[KEY_FN_F9] = "Fn F9";			
	m_keys[KEY_FN_F10] = "Fn F10";
	m_keys[KEY_FN_F11] = "Fn F11";		
	m_keys[KEY_FN_F12] = "Fn F12";
	m_keys[KEY_FN_1] = "Fn 1";			
	m_keys[KEY_FN_2] = "Fn 2";
	m_keys[KEY_FN_D] = "Fn D";			
	m_keys[KEY_FN_E] = "Fn E";
	m_keys[KEY_FN_F] = "Fn F";			
	m_keys[KEY_FN_S] = "Fn S";
	m_keys[KEY_FN_B] = "Fn B";
	m_keys[KEY_BRL_DOT1] = "Braille Dot 1";	
	m_keys[KEY_BRL_DOT2] = "Braille Dot 2";
	m_keys[KEY_BRL_DOT3] = "Braille Dot 3";	
	m_keys[KEY_BRL_DOT4] = "Braille Dot 4";
	m_keys[KEY_BRL_DOT5] = "Braille Dot 5";	
	m_keys[KEY_BRL_DOT6] = "Braille Dot 6";
	m_keys[KEY_BRL_DOT7] = "Braille Dot 7";	
	m_keys[KEY_BRL_DOT8] = "Braille Dot 8";
	m_keys[KEY_BRL_DOT9] = "Braille Dot 9";	
	m_keys[KEY_BRL_DOT10] = "Braille Dot 10";
	m_keys[KEY_NUMERIC_0] = "Numeric 0";		
	m_keys[KEY_NUMERIC_1] = "Numeric 1";
	m_keys[KEY_NUMERIC_2] = "Numeric 2";		
	m_keys[KEY_NUMERIC_3] = "Numeric 3";
	m_keys[KEY_NUMERIC_4] = "Numeric 4";		
	m_keys[KEY_NUMERIC_5] = "Numeric 5";
	m_keys[KEY_NUMERIC_6] = "Numeric 6";		
	m_keys[KEY_NUMERIC_7] = "Numeric 7";
	m_keys[KEY_NUMERIC_8] = "Numeric 8";		
	m_keys[KEY_NUMERIC_9] = "Numeric 9";
	m_keys[KEY_NUMERIC_STAR] = "Numeric *";	
	m_keys[KEY_NUMERIC_POUND] = "Numeric #";
	m_keys[KEY_BATTERY] = "Battery";
	m_keys[KEY_BLUETOOTH] = "Bluetooth";		
	m_keys[KEY_BRIGHTNESS_CYCLE] = "Brightness Cycle";
	m_keys[KEY_BRIGHTNESS_ZERO] = "Brightness Zero"; 
	m_keys[KEY_DASHBOARD] = "Dashboard";
	m_keys[KEY_DISPLAY_OFF] = "Display Off";	
	m_keys[KEY_DOCUMENTS] = "Documents";
	m_keys[KEY_FORWARDMAIL] = "Forward Mail";	
	m_keys[KEY_NEW]  = "New";
	m_keys[KEY_KBDILLUMDOWN] = "Kbd Illum Down";	
	m_keys[KEY_KBDILLUMUP] = "Kbd Illum Up";
	m_keys[KEY_KBDILLUMTOGGLE] = "Kbd Illum Toggle"; 
	m_keys[KEY_REDO] = "Redo";
	m_keys[KEY_REPLY] = "Reply";			
	m_keys[KEY_SAVE] = "Save";
	m_keys[KEY_SCALE] = "Scale";			
	m_keys[KEY_SEND] = "Send";
	m_keys[KEY_SCREENLOCK] = "Screen Lock";	
	m_keys[KEY_SWITCHVIDEOMODE] = "Switch Video Mode";
	m_keys[KEY_UWB] = "UWB";			
	m_keys[KEY_VIDEO_NEXT] = "Video Next";
	m_keys[KEY_VIDEO_PREV] = "Video Prev";	
	m_keys[KEY_WIMAX] = "WIMAX";
	m_keys[KEY_WLAN] = "WLAN";

}
void Touch::init_absval()
{
	m_absval = new char*[6];
	m_absval[0] = "Value";
	m_absval[1] = "Min  ";
	m_absval[2] = "Max  ";
	m_absval[3] = "Fuzz ";
	m_absval[4] = "Flat ";
	m_absval[5] = "Resolution ";
}
void Touch::init_relatives()
{
	m_relatives = new char*[REL_MAX + 10];
	for (int i = 0; i < REL_MAX; i++)
	{
		m_relatives[i] = NULL;
	}
	m_relatives[REL_X] = "X"; 
	m_relatives[REL_Y] = "Y";
	m_relatives[REL_Z] = "Z"; 
	m_relatives[REL_HWHEEL] = "HWheel";
	m_relatives[REL_DIAL] = "Dial"; 
	m_relatives[REL_WHEEL] = "Wheel"; 
	m_relatives[REL_MISC] = "Misc";
}
void Touch::init_absolutes()
{
	m_absolutes = new char*[1000];
	for (int i = 0; i < ABS_MAX; i++)
	{
		m_absolutes[i] = NULL;
	}
	m_absolutes[ABS_X] = "X";
	m_absolutes[ABS_Y] = "Y";
	m_absolutes[ABS_Z] = "Z"; 
	m_absolutes[ABS_RX] = "Rx";
	m_absolutes[ABS_RY] = "Ry"; 
	m_absolutes[ABS_RZ] = "Rz";
	m_absolutes[ABS_THROTTLE] = "Throttle"; 
	m_absolutes[ABS_RUDDER] = "Rudder";
	m_absolutes[ABS_WHEEL] = "Wheel"; 
	m_absolutes[ABS_GAS] = "Gas";
	m_absolutes[ABS_BRAKE] = "Brake"; 
	m_absolutes[ABS_HAT0X] = "Hat0X";
	m_absolutes[ABS_HAT0Y] = "Hat0Y"; 
	m_absolutes[ABS_HAT1X] = "Hat1X";
	m_absolutes[ABS_HAT1Y] = "Hat1Y"; 
	m_absolutes[ABS_HAT2X] = "Hat2X";
	m_absolutes[ABS_HAT2Y] = "Hat2Y"; 
	m_absolutes[ABS_HAT3X] = "Hat3X";
	m_absolutes[ABS_HAT3Y] = "Hat 3Y";
	m_absolutes[ABS_PRESSURE] = "Pressure";
	m_absolutes[ABS_DISTANCE] = "Distance"; 
	m_absolutes[ABS_TILT_X] = "XTilt";
	m_absolutes[ABS_TILT_Y] = "YTilt"; 
	m_absolutes[ABS_TOOL_WIDTH] = "Tool Width";
	m_absolutes[ABS_VOLUME] = "Volume"; 
	m_absolutes[ABS_MISC] = "Misc";
	m_absolutes[ABS_MT_TOUCH_MAJOR] = "Touch Major";
	m_absolutes[ABS_MT_TOUCH_MINOR] = "Touch Minor";
	m_absolutes[ABS_MT_WIDTH_MAJOR] = "Width Major";
	m_absolutes[ABS_MT_WIDTH_MINOR] = "Width Minor";
	m_absolutes[ABS_MT_ORIENTATION] = "Orientation";
	m_absolutes[ABS_MT_POSITION_X] = "Position X";
	m_absolutes[ABS_MT_POSITION_Y] = "Position Y";
	m_absolutes[ABS_MT_TOOL_TYPE] = "Tool Type";
	m_absolutes[ABS_MT_BLOB_ID] = "Blob ID";
	m_absolutes[ABS_MT_TRACKING_ID] = "Tracking ID";
}
void Touch::init_misc()
{
	m_misc = new char*[10];
	for (int i = 0; i < MSC_MAX; i++)
	{
		m_misc[i] = NULL;
	}
	m_misc[MSC_SERIAL] = "Serial"; 
	m_misc[MSC_PULSELED] = "Pulseled";
	m_misc[MSC_GESTURE] = "Gesture"; 
	m_misc[MSC_RAW] = "RawData";
	m_misc[MSC_SCAN] = "ScanCode";
}
void Touch::init_leds()
{
	m_leds = new char*[20];
	for (int i = 0; i < LED_MAX; i++)
	{
		m_leds[i] = NULL;
	}
	m_leds[LED_NUML] = "NumLock"; 
	m_leds[LED_CAPSL] = "CapsLock"; 
	m_leds[LED_SCROLLL] = "ScrollLock"; 
	m_leds[LED_COMPOSE] = "Compose";
	m_leds[LED_KANA] = "Kana";
	m_leds[LED_SLEEP] = "Sleep";
	m_leds[LED_SUSPEND] = "Suspend"; 
	m_leds[LED_MUTE] = "Mute";
	m_leds[LED_MISC] = "Misc";
}
void Touch::init_repeats()
{
	m_repeats = new char*[5];
	for (int i = 0; i < REP_MAX; i++)
	{
		m_repeats[i] = NULL;
	}
	m_repeats[REP_DELAY] = "Delay";
	m_repeats[REP_PERIOD] = "Period";
}
void Touch::init_sounds()
{
	m_sounds = new char*[10];
	for (int i = 0; i < SND_MAX; i++)
	{
		m_sounds[i] = NULL;
	}
	m_sounds[SND_CLICK] = "Click";
	m_sounds[SND_BELL] = "Bell";
	m_sounds[SND_TONE] = "Tone";
}
void Touch::init_names()
{
	m_names = new char**[50];
	for (int i = 0; i < EV_MAX; i++)
	{
		m_names[i] = NULL;
	}
	m_names[EV_SYN] = m_events; 
	m_names[EV_KEY] = m_keys;
	m_names[EV_REL] = m_relatives; 
	m_names[EV_ABS] = m_absolutes;
	m_names[EV_MSC] = m_misc;
	m_names[EV_LED] = m_leds;
	m_names[EV_SND] = m_sounds; 
	m_names[EV_REP] = m_repeats;
}

bool Touch::init_screen()
{
	unsigned short id[4];
	unsigned long bit[EV_MAX][NBITS(KEY_MAX)];
	char name[256] = "Unknown";
	int abs[6] = { 0 };
	
	ioctl(m_fd, EVIOCGNAME(sizeof(name)), name);
	if (std::string(name).compare(std::string("FT5406 memory based driver")) != 0)
		return false;
	
	printf("Input device name: \"%s\"\n", name);
	
	memset(bit, 0, sizeof(bit));
	ioctl(m_fd, EVIOCGBIT(0, EV_MAX), bit[0]);
	printf("Supported events:\n");
	
	int i, j, k;
	for (i = 0; i < EV_MAX; i++)
	{
		if (!test_bit(i, bit[0]))
			continue;
		printf("  Event type %d (%s)\n", i, m_events[i] ? m_events[i] : "?");
		if (!i)
			continue;
		ioctl(m_fd, EVIOCGBIT(i, KEY_MAX), bit[i]);
		
		for (j = 0; j < KEY_MAX; j++)
		{
			if (!test_bit(j, bit[i]))
				continue;
			printf("    Event code %d (%s)\n", j, m_names[i] ? (m_names[i][j] ? m_names[i][j] : "?") : "?");
			if (i != EV_ABS)
				continue;
			ioctl(m_fd, EVIOCGABS(j), abs);
			
			for (k = 0; k < 5; k++)
			{
				if (!((k < 3) || abs[k]))
					continue;
				printf("    %s %6d\n", m_absval[k], abs[k]);
				
				if (j == 0)
				{
					if (m_absval[k] == "Min  ") m_screen_x_min = abs[k];
					if (m_absval[k] == "Max  ") m_screen_x_max = abs[k];
				}
				if (j == 1)
				{
					if (m_absval[k] == "Min  ") m_screen_y_min = abs[k];
					if (m_absval[k] == "Max  ") m_screen_y_max = abs[k];
				}
			}
		}
	}
}

int Touch::get_max(int* arr, int len)
{
	int max = -1;
	
	for (int i = 0; i < len; i++) {
		if (arr[i] > max)
			max = arr[i];
	}
	
	return max;
}
#include "section.h"

using namespace std;

Section::Section(int id, UPDATE_METHOD* updater, SET_METHOD* setter) {
	m_id = id;
	m_update_method = updater;
	m_set_method = setter;
}

void Section::update() {
	(*m_update_method)(m_id);
}

void Section::set_section(int id) {
	(*m_set_method)(id);
}
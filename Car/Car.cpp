#include <ctime>
#include <iostream>	 
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/sysinfo.h>
#include <sys/time.h>	 
#include <sys/types.h>
#include <unistd.h>
#include <vector>	  
					
#include "char.h" 	
#include "CString.h"
#include "font.h" 
#include "image.h" 
#include "line.h"	  
#include "music_lib.h"
#include "screen.h"
#include "touch.h"
#include "section.h"
#include "list.h"
#include "usbmanager.h"
#include "bluetooth_player.h"
#include "globals.h"

using namespace std;
using namespace FMOD;

#define LOG_FPS

struct button {
	int x;
	int y;
	int width;
	int height;
};
struct i_data {	
	button btnR1, btnR2, btnR3, btnR4;
	button btnL1, btnL2, btnL3, btnL4;
	
	bool inputR1, inputR2, inputR3, inputR4;
	bool inputL1, inputL2, inputL3, inputL4;   
};

//Systems data
i_data input_data;

//Globals
Screen* c;
Touch* t;
bool running = true;
int cur_section = -1;
Section* sections[5];
vector<Drawable*> cur_sec_drawables;

//Fonts
Font* font_serif_16;
Font* font_serif_12;

bool check_btn(button& b, int x, int y) {
	if (x < b.x)
		return false;
	if (y < b.y)
		return false;
	
	if (x > b.x + b.width)
		return false;
	if (y > b.y + b.height)
		return false;
	
	return true;
}
void update_section(int new_section) {
	for (int i = 0; i < cur_sec_drawables.size(); i++) {
		Screen::get_instance()->remove_drawable(cur_sec_drawables.at(i));
	}
	
	cur_sec_drawables.clear();
	
	vector<Drawable*>* n_d = sections[new_section]->get_drawables();
	
	for (int i = 0; i < n_d->size(); i++) {
		cur_sec_drawables.push_back(n_d->at(i));
		Screen::get_instance()->add_drawable(n_d->at(i));
	}
	
	cur_section = new_section;
}

void update_cur_sec(int id) {
	if(cur_section == id)
		update_section(id);
}

void* render_thread(void* arg) {
#ifdef LOG_FPS
	int num_times = 10;
	vector<double> times;
	struct timeval begin, end;
#endif
	while (running) {
#ifdef LOG_FPS
		gettimeofday(&begin, NULL);
#endif
		Screen::get_instance()->redraw();
#ifdef LOG_FPS
		gettimeofday(&end, NULL);
		
		double elapsed = (end.tv_sec - begin.tv_sec) + 
              ((end.tv_usec - begin.tv_usec) / 1000000.0);
		
		times.push_back(elapsed);
		if (times.size() == num_times) {
			double avg = 0;
			for (int i = 0; i < num_times; i++)
				avg += times.at(i);
			avg /= num_times;
			printf("FPS: %f\n", 1.0f / avg);
			times.clear();
		}	   
#endif
	}
}
void* music_thread(void* arg) {
	while (running) {
		((MusicSection*)sections[SECTION_MUSIC])->update_music_system();	   
		UsbManager::get_instance()->update();
		//BluetoothPlayer::get_instance()->update();
		sleep(1);
	}
}

void settings_exit_handler() {
	running = false;
}
																																			
int main(int argc, char *argv[]){
	srand(time(NULL));

	UsbManager::get_instance()->update();
	
	//Display
	c = Screen::get_instance();
	t = new Touch();

	//Background Stuff
	Image* b = Image::load(0, 0, "/home/pi/car/system/images/background.rif");
	c->add_drawable(b);
		 
	font_serif_16 = Font::load("/home/pi/car/system/fonts/serif_16.rff");
	font_serif_12 = Font::load("/home/pi/car/system/fonts/serif_12.rff");
	
	//left
	Line l_v(BUTTON_WIDTH, 0, c->get_height(), false, 255, 255, 255, 255);
	Line l_h_1(0, BUTTON_HEIGHT * 1, BUTTON_WIDTH, true, 255, 255, 255, 255);
	Line l_h_2(0, BUTTON_HEIGHT * 2, BUTTON_WIDTH, true, 255, 255, 255, 255);
	Line l_h_3(0, BUTTON_HEIGHT * 3, BUTTON_WIDTH, true, 255, 255, 255, 255);
	
	//right
	Line r_v(c->get_width() - BUTTON_WIDTH, 0, c->get_height(), false, 255, 255, 255, 255);
	Line r_h_1(c->get_width() - BUTTON_WIDTH, BUTTON_HEIGHT * 1, BUTTON_WIDTH, true, 255, 255, 255, 255);
	Line r_h_2(c->get_width() - BUTTON_WIDTH, BUTTON_HEIGHT * 2, BUTTON_WIDTH, true, 255, 255, 255, 255);
	Line r_h_3(c->get_width() - BUTTON_WIDTH, BUTTON_HEIGHT * 3, BUTTON_WIDTH, true, 255, 255, 255, 255);
	
	//Input
	//Left Buttons
	input_data.btnL1.x = 0;
	input_data.btnL1.y = BUTTON_HEIGHT * 0;
	input_data.btnL1.width = BUTTON_WIDTH;
	input_data.btnL1.height = BUTTON_HEIGHT;
	input_data.btnL2.x = 0;
	input_data.btnL2.y = BUTTON_HEIGHT * 1;
	input_data.btnL2.width = BUTTON_WIDTH;
	input_data.btnL2.height = BUTTON_HEIGHT;
	input_data.btnL3.x = 0;
	input_data.btnL3.y = BUTTON_HEIGHT * 2;
	input_data.btnL3.width = BUTTON_WIDTH;
	input_data.btnL3.height = BUTTON_HEIGHT;
	input_data.btnL4.x = 0;
	input_data.btnL4.y = BUTTON_HEIGHT * 3;
	input_data.btnL4.width = BUTTON_WIDTH;
	input_data.btnL4.height = BUTTON_HEIGHT;
	
	//Right Buttons
	input_data.btnR1.x = c->get_width() - BUTTON_WIDTH;
	input_data.btnR1.y = BUTTON_HEIGHT * 0;
	input_data.btnR1.width = BUTTON_WIDTH;
	input_data.btnR1.height = BUTTON_HEIGHT;
	input_data.btnR2.x = c->get_width() - BUTTON_WIDTH;
	input_data.btnR2.y = BUTTON_HEIGHT * 1;
	input_data.btnR2.width = BUTTON_WIDTH;
	input_data.btnR2.height = BUTTON_HEIGHT;
	input_data.btnR3.x = c->get_width() - BUTTON_WIDTH;
	input_data.btnR3.y = BUTTON_HEIGHT * 2;
	input_data.btnR3.width = BUTTON_WIDTH;
	input_data.btnR3.height = BUTTON_HEIGHT;
	input_data.btnR4.x = c->get_width() - BUTTON_WIDTH;
	input_data.btnR4.y = BUTTON_HEIGHT * 3;
	input_data.btnR4.width = BUTTON_WIDTH;
	input_data.btnR4.height = BUTTON_HEIGHT;
	
	c->add_drawable(&l_v);
	c->add_drawable(&l_h_1);
	c->add_drawable(&l_h_2);
	c->add_drawable(&l_h_3);
	
	c->add_drawable(&r_v);
	c->add_drawable(&r_h_1);
	c->add_drawable(&r_h_2);
	c->add_drawable(&r_h_3);
	
	//Left string
	CString str_home(font_serif_16, 25, 45, "Home");
	CString str_radio(font_serif_16, 25, 165, "Radio");
	CString str_music(font_serif_16, 25, 285, "Music");
	CString str_settings(font_serif_16, 5, 405, "Settings");
	c->add_drawable(&str_home);
	c->add_drawable(&str_radio);
	c->add_drawable(&str_music);
	c->add_drawable(&str_settings);
	
	pthread_t render_t;
	pthread_create(&render_t, NULL, render_thread, NULL);

	sections[SECTION_HOME] = new HomeSection(c->get_width(), c->get_height(), SECTION_HOME, &update_cur_sec, &update_section, font_serif_16);
	sections[SECTION_RADIO] = new RadioSection(SECTION_RADIO, &update_cur_sec, &update_section, font_serif_12);
	sections[SECTION_MUSIC] = new MusicSection(SECTION_MUSIC, &update_cur_sec, &update_section, BUTTON_WIDTH, BUTTON_HEIGHT, font_serif_12);
	sections[SECTION_SETTINGS] = new SettingsSection(SECTION_SETTINGS, &update_cur_sec, &update_section, &settings_exit_handler, font_serif_12, BUTTON_WIDTH, BUTTON_HEIGHT);
	sections[SECTION_BLUETOOTH] = new BluetoothSection(SECTION_BLUETOOTH, &update_cur_sec, &update_section, font_serif_12);
	
	update_section(SECTION_HOME);
	
	pthread_t music_t;
	pthread_create(&music_t, NULL, music_thread, NULL);
	
	while (running)
	{
		int x, y, type;
		t->get_sample(&x, &y, &type); 
		
		input_data.inputL1 = check_btn(input_data.btnL1, x, y) && type == TOUCH_START;
		input_data.inputL2 = check_btn(input_data.btnL2, x, y) && type == TOUCH_START;
		input_data.inputL3 = check_btn(input_data.btnL3, x, y) && type == TOUCH_START;
		input_data.inputL4 = check_btn(input_data.btnL4, x, y) && type == TOUCH_START;
		
		input_data.inputR1 = check_btn(input_data.btnR1, x, y) && type == TOUCH_START;
		input_data.inputR2 = check_btn(input_data.btnR2, x, y) && type == TOUCH_START;
		input_data.inputR3 = check_btn(input_data.btnR3, x, y) && type == TOUCH_START;
		input_data.inputR4 = check_btn(input_data.btnR4, x, y) && type == TOUCH_START;
			
		if (input_data.inputL1)	
			update_section(SECTION_HOME);
			
		if (input_data.inputL2)	
			update_section(SECTION_RADIO);
			
		if (input_data.inputL3)	
			update_section(SECTION_MUSIC);
			
		if (input_data.inputL4)	
			update_section(SECTION_SETTINGS);
			
		section_input s_i;
		s_i.input1 = input_data.inputR1;
		s_i.input2 = input_data.inputR2;
		s_i.input3 = input_data.inputR3;
		s_i.input4 = input_data.inputR4;
			
		sections[cur_section]->update_input(s_i); 
	}		 
	
	return 0;
}
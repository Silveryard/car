#ifndef _CAR_FRAMEBUFFER_H_
#define _CAR_FRAMEBUFFER_H_

class Framebuffer
{	
private:
	static struct fb_fix_screeninfo s_fix;
	static struct fb_var_screeninfo s_var;
	
	char* m_fbp;
	char* m_bbp;
	int m_fb;
	long int m_screensize;
	int m_res_x;
	int m_res_y;
	
	long m_max_pos;
	long m_min_pos;

public:
	Framebuffer();
	~Framebuffer();
	
	int get_res_x();
	int get_res_y();

	int create_color(int r, int g, int b, int a);
	void get_rgba(int c, int* r, int* g, int* b, int* a);

	void set_pixel(int x, int y, int c);
	void set_pixel_alpha(int x, int y, int c);
	void set_row(int x, int y, int len, int* data);
	void set_rect_alpha(int x, int y, int width, int height, int* data);
	void fill(int* data);
	void swap_buffers();
};

#endif

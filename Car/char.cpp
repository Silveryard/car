#include "char.h"

#include "stdlib.h"
#include "stdio.h"
#include <vector>
#include <cstring>

using namespace std;

Char::Char(Font* f, int x, int y, char c, int r, int g, int b, int a)
{
	m_c = c;
	m_x = x;
	m_y = y;
	m_r = r;
	m_g = g;
	m_b = b;
	m_a = a;
	m_f = f;

	m_cmd_set = NULL;
	m_cmd_count = 0;
	
	update();
}

void Char::update_char(char  c)
{
	m_c = c;
	
	update();
}
void Char::update_pos(int x, int y)
{
	m_x = x;
	m_y = y;
	
	update();
}
void Char::update_col(int r, int g, int b, int a)
{
	m_r = r;
	m_g = g;
	m_b = b;
	m_a = a;
	
	update();
}

void Char::get_commands(drawable_cmd** commands, int* count) {
	*commands = m_cmd_set;
	*count = m_cmd_count;
}

void Char::update()
{
	char c_s[1];
	c_s[0] = m_c;
	type = strcat("Char: ", c_s);

	if (m_cmd_set != NULL) {
		for (int i = 0; i < m_cmd_count; i++)
			delete m_cmd_set[i].data;
		delete m_cmd_set;
	}
	m_cmd_count = 0;
	
	vector<drawable_cmd> cmds;

	char_data* cd = m_f->get_char(m_c);
	for (int x = 0; x < cd->w; x++) {
		for (int y = 0; y < cd->h; y++) {
			int i = x + y * cd->w;

			if (cd->data[i].a == 0)
				continue;

			drawable_cmd n_p;
			if (cd->data[i].a == 255)
				n_p.type = DRAWABLE_CMD_PIXEL;
			else
				n_p.type = DRAWABLE_CMD_PIXEL_ALPHA;
			n_p.x = m_x + x;
			n_p.y = m_y + y;
			n_p.width = 1;
			n_p.height = 1;
			n_p.data = new int[1];

			cd = m_f->get_char(m_c);
			n_p.data[0] = Screen::get_instance()->get_color(cd->data[i].r, cd->data[i].g, cd->data[i].b, cd->data[i].a);

			cmds.push_back(n_p);
		}
		//printf("\n");
	}

	m_cmd_set = new drawable_cmd[cmds.size()];

	for (int i = 0; i < cmds.size(); i++)
		m_cmd_set[i] = cmds.at(i);

		
	m_cmd_count = cmds.size();
}
#include "image.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <vector>
#include "stdio.h"

using namespace std;

Image* Image::load(int x, int y, const char* filename)
{
	int fd;

	if ((fd = open(filename, O_RDONLY)) == -1)
	{
		return NULL;
	}

	int len = lseek(fd, -20, SEEK_END);
	lseek(fd, 0, SEEK_SET);

	char* c_width = new char[4];
	char* c_height = new char[4];
	char* buffer = new char[len - 8];

	read(fd, c_width, 4);
	read(fd, c_height, 4);
	int n = read(fd, buffer, len - 8);

	if (n == -1)
	{
		return NULL;
	}

	int i_width = (c_width[0] << 24) | (c_width[1] << 16) | (c_width[2] << 8) | (c_width[3]);
	int i_height = (c_height[0] << 24) | (c_height[1] << 16) | (c_height[2] << 8) | (c_height[3]);

	Image* img = new Image(x, y, i_width, i_height, buffer);
	return img;
}

Image::Image(int px, int py, int width, int height, char* buffer)
{
	m_x = px;
	m_y = py;
	m_w = width;
	m_h = height;

	m_data = new int[m_w * m_h];

	m_cmd_set = NULL;
	m_cmd_count = 0;

	for (int x = 0; x < width; x++)
	{
		for (int y = 0; y < height; y++)
		{
			int p_i = x + y * width;
			int c_i = (x * height + y) * 4;

			int r = buffer[c_i + 0];
			int g = buffer[c_i + 1];
			int b = buffer[c_i + 2];
			int a = buffer[c_i + 3];
			
			int c = Screen::get_instance()->get_color(r, g, b, a);

			m_data[p_i] = c;
		}
	}

	update();
}

void Image::set_pos(int x, int y) {
	m_x = x;
	m_y = y;

	update();
}
int Image::get_width()
{
	return m_w;
}
int Image::get_height()
{
	return m_h;
}

void Image::get_commands(drawable_cmd** commands, int* count) {
	*commands = m_cmd_set;
	*count = m_cmd_count;
}

void Image::update() {
	type = "Image";

	if (m_cmd_set != NULL) {
		for (int i = 0; i < m_cmd_count; i++) {
			delete m_cmd_set[i].data;
		}
		delete m_cmd_set;
	}

	m_cmd_count = 0;

	if (m_w == Screen::get_instance()->get_width() && m_h == Screen::get_instance()->get_height() &&
		m_x == 0 && m_y == 0) {
		m_cmd_set = new drawable_cmd[1];
		m_cmd_count = 1;

		m_cmd_set[0].type = DRAWABLE_CMD_FILL;
		m_cmd_set[0].x = m_x;
		m_cmd_set[0].y = m_y;
		m_cmd_set[0].width = m_w;
		m_cmd_set[0].height = m_h;
		m_cmd_set[0].data = new int[m_w * m_h];

		for (int x = 0; x < m_w; x++) {
			for (int y = 0; y < m_h; y++) {
				int i = x + y * m_w;
				m_cmd_set[0].data[i] = m_data[i];
			}
		}
	}
	else {
		vector<drawable_cmd> cmds;
		int line_x = -1;
		int line_y = -1;
		int line_length = 0;

		for (int y = 0; y < m_h; y++) {
			for (int x = 0; x < m_w; x++) {
				int i = x + y * m_w;
				int r, g, b, a;

				Screen::get_instance()->get_rgba(m_data[i], &r, &g, &b, &a);

				/*if (a == 255) {//Pixel can be part of opaque line?
					if (line_x == -1 || line_y == -1) {//No current line?
						//Create new line from cur pos
						line_x = x;
						line_y = y;
						line_length = 0;
					}

					line_length++;
				}
				else {
				
					if (line_x != -1 && line_y != -1) {
						//Finish cur line
						drawable_cmd n_l;
						n_l.type = DRAWABLE_CMD_ROW;
						n_l.x = line_x;
						n_l.y = line_y;
						n_l.width = line_length;
						n_l.height = 1;
						n_l.data = new int[line_length];

						for (int lx = line_x; lx < line_x + line_length; lx++) {
							int li = (lx + line_y * m_w);
							n_l.data[lx - line_x] = m_data[li];
						}
						cmds.push_back(n_l);
					}
					*/
					//Discard blank pixels
					if (a == 0)
						continue;

					drawable_cmd n_p;
					n_p.type = a == 255 ? DRAWABLE_CMD_PIXEL : DRAWABLE_CMD_PIXEL_ALPHA;
					n_p.x = m_x + x;
					n_p.y = m_y + y;
					n_p.width = 1;
					n_p.height = 1;
					n_p.data = new int[0];
					n_p.data[0] = m_data[x + y * m_w];

					cmds.push_back(n_p);
				//}
			}
			/*
			if (line_x != -1 && line_y != -1) {
				//Finish cur line
				drawable_cmd n_l;
				n_l.type = DRAWABLE_CMD_ROW;
				n_l.x = line_x;
				n_l.y = line_y;
				n_l.width = line_length;
				n_l.height = 1;
				n_l.data = new int[line_length];

				for (int lx = line_x; lx < line_x + line_length; lx++) {
					int li = (lx + line_y + m_w);
					n_l.data[lx - line_x] = m_data[li];
				}
				cmds.push_back(n_l);
			}*/
		}

		m_cmd_set = new drawable_cmd[cmds.size()];

		for (int i = 0; i < cmds.size(); i++)
			m_cmd_set[i] = cmds.at(i);

		m_cmd_count = cmds.size();
	}
}
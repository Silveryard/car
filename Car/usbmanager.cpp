#include "usbmanager.h"

#include <unistd.h>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <sys/stat.h>
#include <scsi/scsi.h>
#include <scsi/sg.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <fcntl.h>

#include <string.h>
#include <limits.h>
#include <sstream>

#include "helper.h"

using namespace std;

UsbManager* UsbManager::s_instance;

UsbManager* UsbManager::get_instance() {
	if (s_instance == NULL) {
		s_instance = new UsbManager();
	}
	
	return s_instance;
}

UsbManager::UsbManager() {
	m_mounted_drives = vector<usb_drive*>();
	m_mounted_listeners = vector<USB_DRIVE_MOUNTED*>();
	m_unmounted_listeners = vector<USB_DRIVE_UNMOUNTED*>();
}
UsbManager::~UsbManager() {
	for (int i = 0; i < m_mounted_drives.size(); i++) {
		delete m_mounted_drives.at(i);
	}
}

void UsbManager::register_mounted_listener(USB_DRIVE_MOUNTED* listener) {
	m_mounted_listeners.push_back(listener);
}
void UsbManager::unregister_mounted_listener(USB_DRIVE_MOUNTED* listener) {
	for (vector<USB_DRIVE_MOUNTED*>::iterator it = m_mounted_listeners.begin(); it < m_mounted_listeners.end(); it++) {
		if ((*it) == listener) {
			m_mounted_listeners.erase(it);
			return;
		}
	}
}

void UsbManager::register_unmounted_listener(USB_DRIVE_UNMOUNTED* listener) {
	m_unmounted_listeners.push_back(listener);
}
void UsbManager::unregister_unmounted_listener(USB_DRIVE_UNMOUNTED* listener) {
	for (vector<USB_DRIVE_MOUNTED*>::iterator it = m_unmounted_listeners.begin(); it < m_unmounted_listeners.end(); it++) {
		if ((*it) == listener) {
			m_unmounted_listeners.erase(it);
			return;
		}
	}
}

vector<usb_drive*>* UsbManager::get_mounted_drives() {
	return &m_mounted_drives;
}

void UsbManager::update() {
	vector<char*> devices;
	get_mounted_device_paths(&devices);
	map<char*, char*> serials = map<char*, char*>();
	
	//Get Serials
	for (int i = 0; i < devices.size(); i++) {
		char* serial = get_serial(devices.at(i));
		serials.insert(pair<char*, char*>(devices.at(i), serial));
	}
	
	//Check for unmounts
	vector<vector<usb_drive*>::iterator> deletables;
	for (vector<usb_drive*>::iterator it = m_mounted_drives.begin(); it < m_mounted_drives.end(); it++) {
		usb_drive* drive = *it;
		string md_s(drive->serial);
		
		bool found = false;
		for (int j = 0; j < devices.size(); j++) {
			string d_s(serials.at(devices.at(j)));
			
			if (d_s.compare(md_s) != 0)
				continue;
			
			found = true;
			break;
		}
		
		if (found)
			continue;
		
		printf("Removed USB drive: \n");				  
		printf("    Device Path: '%s'\n", drive->device_path);
		printf("    Serial:      '%s'\n", drive->serial);
		printf("    Mount Path:  '%s'\n", drive->mount_path);
		printf("    Name:        '%s'\n", drive->name);
		printf("\n");
			
		for (int k = 0; k < m_unmounted_listeners.size(); k++) {
			USB_DRIVE_UNMOUNTED* listener = m_unmounted_listeners.at(k);
			(*listener)(drive);
		}
			
		deletables.push_back(it);
	}
	for (int i = 0; i < deletables.size(); i++) {
		usb_drive* drive = *(deletables.at(i));
		m_mounted_drives.erase(deletables.at(i));
		delete drive;
	}		 
	
	//Check for mounts
	vector<usb_drive*> addables = vector<usb_drive*>();
	for (int i = 0; i < devices.size(); i++) {
		string d_s(serials.at(devices.at(i)));
		
		bool found = false;
		for (int j = 0; j < m_mounted_drives.size(); j++) {
			string md_s(m_mounted_drives.at(j)->serial);
			
			if (d_s.compare(md_s) != 0)
				continue;
			
			found = true;
			break;
		}
		
		if (found)
			continue;
		
		usb_drive* n_d = new usb_drive();
		n_d->device_path = devices.at(i);
		n_d->serial = serials.at(devices.at(i));
		n_d->mount_path = get_mount_path(devices.at(i));
		n_d->name = get_name(devices.at(i));
		
		if (strlen(n_d->mount_path) == 0)
			continue;
		
		addables.push_back(n_d);
		
		printf("Found new USB drive: \n");				  
		printf("    Device Path: '%s'\n", n_d->device_path);
		printf("    Serial:      '%s'\n", n_d->serial);
		printf("    Mount Path:  '%s'\n", n_d->mount_path);
		printf("    Name:        '%s'\n", n_d->name);
		printf("\n");
	}	
	for (int i = 0; i < addables.size(); i++) {
		usb_drive* n_d = addables.at(i);
		m_mounted_drives.push_back(n_d);
		
		for (int j = 0; j < m_mounted_listeners.size(); j++) {
			USB_DRIVE_MOUNTED* listener = m_mounted_listeners.at(j);
			(*listener)(n_d);
		} 
	} 
}

void UsbManager::get_mounted_device_paths(std::vector<char*>* devices) {
	devices->clear();
	
	DIR * d;

    /* Open the directory specified by "dir_name". */

	d = opendir("/dev");

	    /* Check it was opened. */
	if (!d) {
		fprintf(stderr,
			"Cannot open directory '%s': %s\n",
			"/dev",
			strerror(errno));
		exit(EXIT_FAILURE);
	}
	while (1) {
		struct dirent * entry;
		const char * d_name;

		        /* "Readdir" gets subsequent entries from "d". */
		entry = readdir(d);
		if (!entry) {
		    /* There are no more entries in this directory, so break
		       out of the while loop. */
			break;
		}
		d_name = entry->d_name;
		
		if (entry->d_type & DT_DIR) {
			if (strlen(d_name) == 4 && (string(d_name).find("sd")) == 0) {
				char* c = strdup((string("/dev/") + string(d_name)).c_str());
				devices->push_back(c);
			}
		}
	}
	/* After going through all the entries, close the directory. */
	if (closedir(d)) {
		fprintf(stderr,
			"Could not close '%s': %s\n",
			"/dev",
			strerror(errno));
		exit(EXIT_FAILURE);
	}
}
char* UsbManager::get_mount_path(char* device_path) {
	string cmd = string("grep \"") + string(device_path) + string("\" /proc/mounts | cut -d ' ' -f 2");
	return strdup(call_system_read_out(cmd).c_str());
}
char* UsbManager::get_serial(char* device_path) {
	char buf[255];
	int fd;
	
	fd = open(device_path, O_RDONLY | O_NONBLOCK);
	if (fd < 0) {
		perror(device_path);
		return "";
	}
	
	memset(buf, 0, sizeof(buf));
	
	// we shall retrieve page 0x80 as per http://en.wikipedia.org/wiki/SCSI_Inquiry_Command
	unsigned char inq_cmd[] = { INQUIRY, 1, 0x80, 0, 255, 0 };
	unsigned char sense[32];
	struct sg_io_hdr io_hdr;
	int result;

	memset(&io_hdr, 0, sizeof(io_hdr));
	io_hdr.interface_id = 'S';
	io_hdr.cmdp = inq_cmd;
	io_hdr.cmd_len = sizeof(inq_cmd);
	io_hdr.dxferp = buf;
	io_hdr.dxfer_len = 255;
	io_hdr.dxfer_direction = SG_DXFER_FROM_DEV;
	io_hdr.sbp = sense;
	io_hdr.mx_sb_len = sizeof(sense);
	io_hdr.timeout = 5000;

	result = ioctl(fd, SG_IO, &io_hdr);
	if (result < 0)
		return "";

	if ((io_hdr.info & SG_INFO_OK_MASK) != SG_INFO_OK)
		return "";
	
	
	/*char* serial = new char[buf[4] + 1];
	
	for (int i = 0; i < buf[4]; i++) {
		serial[i] = buf[8 + i];
	}	
	
	serial[buf[4]] = '\0';
		*/				   
	char* serial = new char[26];
	
	for (int i = 0; i < 25; i++) {
		serial[i] = buf[8 + i];
	}						 
	
	serial[25] = '\0';
	
	return serial;
}
char* UsbManager::get_name(char* device_path) {
	string cmd = string("find -L /dev/disk/by-label -inum $(stat -c %i ") + string(device_path) + string(") -print");
	char* path = strdup(call_system_read_out(cmd).c_str());
	string name;
	
	for (int i = 0; i < strlen(path); i++) {
		if (path[i] == '/')
			name = string();
		else
			name += path[i];
	}
	
	return strdup(name.c_str());
}
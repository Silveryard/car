#ifndef _CAR_MUSIC_LIB_H_
#define _CAR_MUSIC_LIB_H_

#include <vector>
#include <string.h>
#include "tag.h"
#include "usbmanager.h"

struct Artist;
struct Album;
struct Song;

struct Artist {
	char* name;
	
	std::vector<Album*> albums;
};
struct Album {
	char* name;
	
	Artist* artist;
	std::vector<Song*> songs;
};
struct Song
{
	char* file;
	char* title;
	
	Artist* artist;
	Album* album;
	int track;
};
struct UsbList 
{
	usb_drive* drive;
	std::vector<Artist*> artists;
};

class MusicLib
{
private:
	static MusicLib* s_instance;
	
public:
	typedef void(LIB_UPDATED)();
	static MusicLib* get_instance();
	
private:
	std::vector<LIB_UPDATED*> m_update_listeners;
	
	std::vector<Song*> m_songs;
	std::vector<Artist*> m_artists;
	std::vector<UsbList*> m_usb;
	
public:
	void init();
	
	std::vector<Song*> get_all_songs();
	std::vector<Artist*> get_all_artists();
	
	void add_usb_lib(usb_drive* drive);
	void remove_usb_drive(usb_drive* drive);
	
	void register_update_listener(LIB_UPDATED* listener);
	void unregister_update_listener(LIB_UPDATED* listener);
	
private:
	void update_song_list();
};

#endif

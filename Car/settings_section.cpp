#include "section.h"

#include <string>
#include <sstream>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "globals.h"

using namespace std;

SettingsSection::SettingsSection(int id, UPDATE_METHOD* method, SET_METHOD* setter, HANDLER* exit_handler, Font* serif_12, int button_width, int button_height) : Section(id, method, setter) {
	m_drawables = vector<Drawable*>();
	m_exit_handler = exit_handler;
	
	m_list = new List(button_width + 10);
	m_base_string_x = button_width + 30;
	m_serif_12 = serif_12;
	
	Image* btn_down = Image::load(690, button_height * 3 + 25, "/home/pi/car/system/images/arrow_down.rif");
	Image* btn_up = Image::load(690, button_height * 2 + 25, "/home/pi/car/system/images/arrow_up.rif");
	Image* btn_select = Image::load(685, button_height * 0 + 25, "/home/pi/car/system/images/select.rif");
	Image* btn_back = Image::load(685, button_height * 1 + 25, "/home/pi/car/system/images/back.rif");
	
	m_buttons.push_back(btn_down);
	m_buttons.push_back(btn_up);
	m_buttons.push_back(btn_select);
	m_buttons.push_back(btn_back);
	
	m_home_elements.push_back(new CString(serif_12, m_base_string_x, 20 + (60 * INDEX_HOME_VOLUME), "Volume"));
	m_home_elements.push_back(new CString(serif_12, m_base_string_x, 20 + (60 * INDEX_HOME_SOUND), "Sound"));
	m_home_elements.push_back(new CString(serif_12, m_base_string_x, 20 + (60 * INDEX_HOME_EXIT), "Exit"));
	
	CString* cstr_autoplay;
	CString* cstr_shuffle;
		
	string autoplay_string("Autoplay: ");
	if (GLOBALS_MUSIC_AUTOPLAY)
		autoplay_string += string("True");
	else
		autoplay_string += string("False");
	cstr_autoplay = new CString(m_serif_12, m_base_string_x, 20 + (60 * INDEX_MUSIC_AUTOPLAY), strdup(autoplay_string.c_str()));
		
	string shuffle_string("Shuffle: ");
	if (GLOBALS_MUSIC_SHUFFLE)
		shuffle_string += string("True");
	else
		shuffle_string += string("False");
	cstr_shuffle = new CString(m_serif_12, m_base_string_x, 20 + (60 * INDEX_MUSIC_SHUFFLE), strdup(shuffle_string.c_str()));
		
	m_music_elements.push_back(cstr_autoplay);
	m_music_elements.push_back(cstr_shuffle);

	string volume_string = "Volume: ";
	stringstream out;
	out << GLOBALS_SOUND_VOLUME;
	volume_string.append(out.str());
	volume_string.append("%");
	CString* cstr_vol_stat = new CString(serif_12, m_base_string_x, 20 + (60 + INDEX_VOLUME_STATUS), strdup(volume_string.c_str()));
	CString* cstr_vol_up = new CString(serif_12, m_base_string_x, 20 + (60 + INDEX_VOLUME_STATUS), "Up");
	CString* cstr_vol_dwn = new CString(serif_12, m_base_string_x, 20 + (60 + INDEX_VOLUME_STATUS), "Down");

	m_volume_elements.push_back(cstr_vol_up);
	m_volume_elements.push_back(cstr_vol_stat);
	m_volume_elements.push_back(cstr_vol_dwn);

	set_volume();
	set_mode(MODE_HOME, false);
}

vector<Drawable*>* SettingsSection::get_drawables() {
	m_drawables.clear();
	
	for (int i = 0; i < m_buttons.size(); i++)
		m_drawables.push_back(m_buttons.at(i));
	
	vector<Drawable*> list_drawables = m_list->get_drawables();
	for (int i = 0; i < list_drawables.size(); i++)
		m_drawables.push_back(list_drawables.at(i));
	
	return &m_drawables;
}
void SettingsSection::update_input(section_input input) {
	if (input.input3)
		m_list->move_up();
	
	if (input.input4)
		m_list->move_down();
	
	switch (m_cur_mode) {
	case MODE_HOME: {
		if (input.input1) {
			if (m_list->get_selected_index() == INDEX_HOME_SOUND)
				set_mode(MODE_SOUND);
			else if (m_list->get_selected_index() == INDEX_HOME_EXIT)
				(*m_exit_handler)();
			else if (m_list->get_selected_index() == INDEX_HOME_VOLUME)
				set_mode(MODE_VOLUME);
		}
		break;
	}
	case MODE_SOUND: {
		if (input.input1) {
			if (m_list->get_selected_index() == INDEX_MUSIC_AUTOPLAY) {
				GLOBALS_MUSIC_AUTOPLAY = !GLOBALS_MUSIC_AUTOPLAY;
				string autoplay_string("Autoplay: ");
				if (GLOBALS_MUSIC_AUTOPLAY)
					autoplay_string += string("True");
				else
					autoplay_string += string("False");
				m_music_elements.at(0)->set_string(strdup(autoplay_string.c_str()));
			}
			if (m_list->get_selected_index() == INDEX_MUSIC_SHUFFLE) {
				GLOBALS_MUSIC_SHUFFLE = !GLOBALS_MUSIC_SHUFFLE;
				string shuffle_string("Shuffle: ");
				if (GLOBALS_MUSIC_SHUFFLE)
					shuffle_string += string("True");
				else
					shuffle_string += string("False");
				m_music_elements.at(1)->set_string(strdup(shuffle_string.c_str()));
			}
		}
		
		if (input.input2)
			set_mode(MODE_HOME);
	}
	case MODE_VOLUME: {
		if (input.input1)
		{
			bool update_vol = false;

			if (m_list->get_selected_index() == INDEX_VOLUME_UP)
			{
				GLOBALS_SOUND_VOLUME += VOLUME_STEP;
				if (GLOBALS_SOUND_VOLUME > VOLUME_MAX)
					GLOBALS_SOUND_VOLUME = VOLUME_MAX;

				update_vol = true;
			}

			if (m_list->get_selected_index() == INDEX_VOLUME_DOWN)
			{
				GLOBALS_SOUND_VOLUME -= VOLUME_STEP;
				if (GLOBALS_SOUND_VOLUME < VOLUME_MIN)
					GLOBALS_SOUND_VOLUME = VOLUME_MIN;

				update_vol = true;
			}

			if (update_vol)
			{
				string volume_string = "Volume: ";
				stringstream out;
				out << GLOBALS_SOUND_VOLUME;
				volume_string.append(out.str());
				volume_string.append("%");
				m_volume_elements.at(INDEX_VOLUME_STATUS)->set_string(strdup(volume_string.c_str()));
				set_volume();
			}
		}

		if (input.input2)
			set_mode(MODE_HOME);
	}
	}
}

void SettingsSection::set_mode(MODE mode, bool trigger) {
	m_cur_mode = mode;
	
	vector<CString*> elements;
	
	switch (m_cur_mode) {
	case MODE_HOME: {
		for (int i = 0; i < m_home_elements.size(); i++)
			elements.push_back(m_home_elements.at(i));
		break;
	}
	case MODE_SOUND: {
		for (int i = 0; i < m_music_elements.size(); i++)
			elements.push_back(m_music_elements.at(i));
		break;
	}
	case MODE_VOLUME: {
		for (int i = 0; i < m_volume_elements.size(); i++)
			elements.push_back(m_volume_elements.at(i));
		break;
	}
	}
	
	m_list->set_elements(elements);
	
	if (trigger && m_list->needs_update())
		this->update();
}

void SettingsSection::set_volume()
{
	int fl, fr, bl, br;

	int l, r;

	l = GLOBALS_SOUND_VOLUME * 2 * ((100 - GLOBALS_SOUND_BALANCE) / 100.0f);
	r = GLOBALS_SOUND_VOLUME * 2 * (GLOBALS_SOUND_BALANCE / 100.0f);

	fl = l * 2 * ((100 - GLOBALS_SOUND_FADE) / 100.0f);
	fr = r * 2 * ((100 - GLOBALS_SOUND_FADE) / 100.0f);
	bl = l * 2 * (GLOBALS_SOUND_FADE / 100.0f);
	br = r * 2 * (GLOBALS_SOUND_FADE / 100.0f);

	stringstream ss_fl, ss_fr, ss_bl, ss_br;
	ss_fl << fl;
	ss_fr << fr;
	ss_bl << bl;
	ss_br << br;
	string cmd("amixer -c1 set Speaker ");
	cmd.append(ss_fl.str());
	cmd.append("%,");
	cmd.append(ss_fr.str());
	cmd.append("%,0%,0%,0%,0%,");
	cmd.append(ss_bl.str());
	cmd.append("%,");
	cmd.append(ss_br.str());
	cmd.append("%");

	system(strdup(cmd.c_str()));

	if (fl == 0 || fr == 0 || bl == 0 || br == 0)
	{
		system("amixer -c1 set Speaker mute");
	}
	else
	{
		system("amixer -c1 set Speaker unmute");
	}
}
#ifndef _CAR_CSTRING_H_
#define _CAR_CSTRING_H_

#include "font.h"
#include "screen.h"

class CString : public Drawable
{
private:
	int m_x, m_y;
	char* m_text;
	int m_max_chars;
	Font* m_f;
	
	int m_w, m_h;

	drawable_cmd* m_cmd_set;
	int m_cmd_count;
	
public:
	CString(Font* f, int x, int y, char* m_text, int max_chars = -1);
	
	void set_string(char* text, int max_chars = -1);
	void set_pos(int x, int y);

	int get_width();
	int get_height();
	int get_x();
	int get_y();

	virtual void get_commands(drawable_cmd** commands, int* count);
private:
	void update();
};

#endif

#ifndef _CAR_FONT_H_
#define _CAR_FONT_H_

#include "framebuffer.h"

struct pixel_data {
	char r;
	char g;
	char b;
	char a;
};

struct char_data
{
	char c;
	char w;
	char h;
	pixel_data* data;
};

class Font
{
public:
	static Font* load(char* filename);
	
private:
	char* m_name;
	char  m_size;
	char  m_char_count;
	
	char_data** m_data;
	
public:
	Font(char* name, char size, char char_count, char_data** data);
		
	char* get_name();
	char get_size();
	
	char_data* get_char(char c);
	
	void print();
};

#endif
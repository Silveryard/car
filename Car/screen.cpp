#include "screen.h"

#include <stdio.h>
#include <algorithm>
#include <unistd.h>

using namespace std;

Screen* Screen::s_instance;
Screen* Screen::get_instance() {
	if (s_instance == NULL)
		s_instance = new Screen();

	return s_instance;
}

Screen::Screen()
{
	m_f = Framebuffer();
	m_drawables = vector<Drawable*>();

	for (int i = 0; i < m_f.get_res_x(); i++) {
		for (int j = 0; j < m_f.get_res_y(); j++) {
			m_f.set_pixel(i, j, get_color(0, 0, 0, 1));
		}
	}
	m_f.swap_buffers();
}
Screen::~Screen(){}

void Screen::add_drawable(Drawable* d)
{
	m_drawables_to_add.push_back(d);
}
void Screen::remove_drawable(Drawable* d)
{
	m_drawables_to_rem.push_back(d);
}

void Screen::redraw()
{	
	vector<Drawable*> a = vector<Drawable*>(m_drawables_to_add);
	m_drawables_to_add.clear();
	vector<Drawable*> r = vector<Drawable*>(m_drawables_to_rem);
	m_drawables_to_rem.clear();

	for (int i = 0; i < a.size(); i++)
		m_drawables.push_back(a.at(i));

	for (int i = 0; i < r.size(); i++)
	{
		vector<Drawable*>::iterator it = find(m_drawables.begin(), m_drawables.end(), r.at(i));
	
		if (it != m_drawables.end())
			m_drawables.erase(it);
	}

	for (int i = 0; i < m_drawables.size(); i++) {
		drawable_cmd* cmd_set;
		int cmd_count;

		if (cmd_count == 24000)
			int i = 10;

		m_drawables.at(i)->get_commands(&cmd_set, &cmd_count);

		for (int j = 0; j < cmd_count; j++) {
			drawable_cmd cur = cmd_set[j];

			switch (cur.type) {
			case DRAWABLE_CMD_PIXEL:{
				m_f.set_pixel(cur.x, cur.y, cur.data[0]);
				break;
			}
			case DRAWABLE_CMD_PIXEL_ALPHA: {
				m_f.set_pixel_alpha(cur.x, cur.y, cur.data[0]);
				break;
			}
			case DRAWABLE_CMD_ROW: {
				m_f.set_row(cur.x, cur.y, cur.width, cur.data);
				break;
			}
			case DRAWABLE_CMD_RECT_ALPHA: {
				m_f.set_rect_alpha(cur.x, cur.y, cur.width, cur.height, cur.data);
				break;
			}
			case DRAWABLE_CMD_FILL: {
				m_f.fill(cur.data);
				break;
			}
			}
		}
	}

	m_f.swap_buffers();
}

int Screen::get_color(int r, int g, int b, int a) {
	return m_f.create_color(r, g, b, a);
}
void Screen::get_rgba(int c, int* r, int* g, int* b, int* a) {
	m_f.get_rgba(c, r, g, b, a);
}

int Screen::get_width()
{
	return m_f.get_res_x();
}
int Screen::get_height()
{
	return m_f.get_res_y();
}
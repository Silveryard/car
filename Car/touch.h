#ifndef _CAR_TOUCH_H_
#define _CAR_TOUCH_H_

#define BITS_PER_LONG (sizeof(long) * 8)
#define NBITS(x) ((((x)-1)/BITS_PER_LONG)+1)
#define OFF(x)  ((x)%BITS_PER_LONG)
#define BIT(x)  (1UL<<OFF(x))
#define LONG(x) ((x)/BITS_PER_LONG)
#define test_bit(bit, array)	((array[LONG(bit)] >> OFF(bit)) & 1)

#define TOUCH_START 0
#define TOUCH_HOLD 1
#define TOUCH_END 2

class Touch
{
private:
	char** m_events;
	char** m_keys;
	char** m_absval;
	char** m_relatives;
	char** m_absolutes;
	char** m_misc;
	char** m_leds;
	char** m_repeats;
	char** m_sounds;
	char*** m_names;
	
	int m_fd;
	
	int m_screen_x_min;
	int m_screen_x_max;
	int m_screen_y_min;
	int m_screen_y_max;
	
public: 
	Touch();
	
	void get_sample(int* x_pos, int* y_pos, int* type);
	
private:
	void init_events();
	void init_keys();
	void init_absval();
	void init_relatives();
	void init_absolutes();
	void init_misc();
	void init_leds();
	void init_repeats();
	void init_sounds();
	void init_names();
	
	bool init_screen();
	
	int get_max(int* arr, int len);
};

#endif


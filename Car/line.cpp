#include "line.h"

#include <unistd.h>

Line::Line(int x, int y, int len, bool hor, int r, int g, int b, int a)
{
	m_x = x;
	m_y = y;
	m_len = len;
	m_hor = hor;
	m_r = r;
	m_g = r;
	m_b = b;
	m_a = a;

	m_cmd_set = NULL;
	m_cmd_count = 0;

	update();
}

void Line::update_pos(int x, int y)
{
	m_x = x;
	m_y = y;
	
	update();
}
void Line::update_len(int len)
{
	m_len = len;
	
	update();
}
void Line::update_orientation(bool hor)
{
	m_hor = hor;
	
	update();
}
void Line::update_color(int r, int g, int b, int a)
{
	m_r = r;
	m_g = g;
	m_b = b;
	m_a = a;
	
	update();
}

int Line::get_x() {
	return m_x;
}
int Line::get_y() {
	return m_y;
}
int Line::get_len() {
	return m_len;
}

void Line::get_commands(drawable_cmd** commands, int* count) {
	*commands = m_cmd_set;
	*count = m_cmd_count;
}

void Line::update()
{
	type = "Line";

	if (m_cmd_set != NULL) {
		for (int i = 0; i < m_cmd_count; i++)
			delete m_cmd_set[i].data;
		delete m_cmd_set;
	}

	if (m_a == 0) {
		m_cmd_set = new drawable_cmd[0];
		m_cmd_count = 0;
		return;
	}

	if (m_hor && m_a == 255) {
		m_cmd_set = new drawable_cmd[1];

		m_cmd_set[0].type = DRAWABLE_CMD_ROW;
		m_cmd_set[0].x = m_x;
		m_cmd_set[0].y = m_y;
		m_cmd_set[0].width = m_len;
		m_cmd_set[0].height = 1;
		m_cmd_set[0].data = new int[m_len];

		for (int x = 0; x < m_len; x++) {
			m_cmd_set[0].data[x] = Screen::get_instance()->get_color(m_r, m_g, m_b, m_a);
		}

		m_cmd_count = 1;
	}
	else {
		m_cmd_count = 0;
		m_cmd_set = new drawable_cmd[m_len];

		for (int i = 0; i < m_len; i++) {
			m_cmd_set[i].type = m_a == 255 ? DRAWABLE_CMD_PIXEL : DRAWABLE_CMD_PIXEL_ALPHA;
			m_cmd_set[i].x = m_x;
			m_cmd_set[i].y = m_y + i;
			m_cmd_set[i].width = 1;
			m_cmd_set[i].height = 1;
			m_cmd_set[i].data = new int[1];

			m_cmd_set[i].data[0] = Screen::get_instance()->get_color(m_r, m_g, m_b, m_a);
		}

		m_cmd_count = m_len;
	}
}
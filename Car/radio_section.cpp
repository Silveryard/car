#include "section.h"

#include <string>
#include <sstream>
#include <stdlib.h>
#include "globals.h"

using namespace std;

RadioSection::RadioSection(int id, UPDATE_METHOD* method, SET_METHOD* setter, Font* serif_12) : Section(id, method, setter) {
	system("/home/pi/car/i2c-init");
	system("/home/pi/car/rdspi reset");
	system("amixer -c1 sset Mic on");
	system("amixer -c1 sset Mic 100%");
	//Aux
	system("amixer -c1 sset Line on");
	system("amixer -c1 sset Line 100%");
	set_mute(true);

	m_drawables = vector<Drawable*>();
	m_senders = vector<sender>();

	m_list = new List(BUTTON_WIDTH + 10);

	sender radio_gong;
	radio_gong.name = new CString(serif_12, BUTTON_WIDTH + 20, 0, "Radio Gong");
	radio_gong.tune = "106.90";
	sender radio_galaxy;
	radio_galaxy.name = new CString(serif_12, BUTTON_WIDTH + 20, 0, "Radio Galaxy");
	radio_galaxy.tune = "88.10";
	sender radio_swr3;
	radio_swr3.name = new CString(serif_12, BUTTON_WIDTH + 20, 0, "SWR3");
	radio_swr3.tune = "103.0";

	m_senders.push_back(radio_gong);
	m_senders.push_back(radio_galaxy);
	m_senders.push_back(radio_swr3);

	vector<CString*> list_elements ;
	for(int i = 0; i < m_senders.size(); i++)
		list_elements.push_back(m_senders.at(i).name);

	m_img_down = Image::load(690, BUTTON_HEIGHT * 3 + 25, "/home/pi/car/system/images/arrow_down.rif");
	m_img_up = Image::load(690, BUTTON_HEIGHT * 2 + 25, "/home/pi/car/system/images/arrow_up.rif");
	m_img_play = Image::load(690, BUTTON_HEIGHT * 1 + 25, "/home/pi/car/system/images/play.rif");
	m_img_pause = Image::load(690, BUTTON_HEIGHT * 1 + 25, "/home/pi/car/system/images/pause.rif");
	m_img_select = Image::load(685, BUTTON_HEIGHT * 0 + 25, "/home/pi/car/system/images/select.rif");

	m_list->set_elements(list_elements);
}

vector<Drawable*>* RadioSection::get_drawables() {
	m_drawables.clear();

	vector<Drawable*> list_elements = m_list->get_drawables();
	for (int i = 0; i < list_elements.size(); i++)
		m_drawables.push_back(list_elements.at(i));

	m_drawables.push_back(m_img_down);
	m_drawables.push_back(m_img_up);
	m_drawables.push_back(m_img_select);

	if (m_muted)
		m_drawables.push_back(m_img_play);
	else
		m_drawables.push_back(m_img_pause);

	return &m_drawables;
}
void RadioSection::update_input(section_input input) {
	if (input.input1)
	{
		set_sender(m_senders.at(m_list->get_selected_index()));
	}

	if (input.input2)
		set_mute(!m_muted);

	if (input.input3)
		m_list->move_up();

	if (input.input4)
		m_list->move_down();
}

void RadioSection::set_sender(sender s)
{
	set_mute(false);
	string tune_str("/home/pi/car/rdspi tune ");
	tune_str.append(s.tune);
	system(tune_str.c_str());
}
void RadioSection::set_volume(int vol)
{
	stringstream out;
	out << "/home/pi/car/rdspi volume ";
	out << vol;
	string s  = out.str();
	system(s.c_str());

	bool n_m = vol == 0;
	bool u = n_m != m_muted;
	m_muted = n_m;
	if (u)
		this->update();
}
void RadioSection::set_mute(bool mute)
{
	set_volume(mute ? 0 : 10);
}
#ifndef _CAR_CHAR_H_
#define _CAR_CHAR_H_

#include "screen.h"
#include "font.h"

class Char : public Drawable
{
private:
	char m_c;
	int m_x;
	int m_y;
	int m_r;
	int m_g;
	int m_b;
	int m_a;
	Font* m_f;
	
	drawable_cmd* m_cmd_set;
	int m_cmd_count;
	
public:
	Char(Font* f, int x, int y, char c, int r, int g, int b, int a);
	
	void update_char(char c);
	void update_pos(int x, int y);
	void update_col(int r, int g, int b, int a);
	
	virtual void get_commands(drawable_cmd** commands, int* count);
	
private:
	void update();
};

#endif

#include "bluetooth_player.h"
				   
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "helper.h"

using namespace std;

BluetoothPlayer* BluetoothPlayer::s_instance;
BluetoothPlayer* BluetoothPlayer::get_instance() {
	if (s_instance == NULL) {
		s_instance = new BluetoothPlayer();
	}
	
	return s_instance;
}

bluetooth_result BluetoothPlayer::is_connected() {
	string call_res = call_system_read_out("qdbus org.bluez");
	vector<string> split = split_str(call_res, '\n');
	
	for (int i = 0; i < split.size(); i++) {
		if (!str_ends_with(split.at(i), "player0"))
			continue;
		
		vector<string> slash_split = split_str(split.at(i), '/');
		vector<string> us_split = split_str(slash_split.at(slash_split.size() - 2), '_');
		
		m_device_mac = string("");
		for (int j = 1; j < us_split.size(); j++) {
			m_device_mac.append(us_split.at(j));
			
			if (j < us_split.size() - 1)
				m_device_mac += "_";
		}
		
		return BLUETOOTH_RESULT_OK;
	}
	
	m_device_mac = string("");
	return BLUETOOTH_RESULT_NOT_CONNECTED;
}
bluetooth_result BluetoothPlayer::get_track_info(char** album, char** artist, char** title, int* duration) {
	if (is_connected() == BLUETOOTH_RESULT_NOT_CONNECTED)
		return BLUETOOTH_RESULT_NOT_CONNECTED;
	
	string call_res = call_cmd(METHOD_TRACK);
	vector<string> lines = split_str(call_res, '\n');
	
	for (int i = 0; i < lines.size(); i++) {
		vector<string> colon_split = split_str(lines.at(i), ':');
		
		if (colon_split.size() < 2)
			continue;
		
		string val = colon_split.at(1).substr(1, colon_split.at(1).size() - 1);
		char* c_val = strdup(val.c_str());
		
		if (colon_split.at(0).compare("Album") == 0) {
			(*album) = c_val;
			continue;
		}
		if (colon_split.at(0).compare("Artist") == 0) {
			(*artist) = c_val;
			continue;
		}
		if (colon_split.at(0).compare("Title") == 0) {
			(*title) = c_val;
			continue;
		}
		if (colon_split.at(0).compare("Duration") == 0) {
			char* pEnd;
			(*duration) = strtol(c_val, &pEnd, 10);
			continue;
		}
	}
	
	return BLUETOOTH_RESULT_OK;
}
bluetooth_result BluetoothPlayer::get_pos(int* pos) {
	if (is_connected() == BLUETOOTH_RESULT_NOT_CONNECTED)
		return BLUETOOTH_RESULT_NOT_CONNECTED;
	
	char* pEnd;
	string res = call_cmd(METHOD_POS);
	(*pos) = strtol(strdup(res.c_str()), &pEnd, 10);
	return BLUETOOTH_RESULT_OK;
}
bluetooth_result BluetoothPlayer::get_status(bluetooth_status* status) {
	if (is_connected() == BLUETOOTH_RESULT_NOT_CONNECTED)
		return BLUETOOTH_RESULT_NOT_CONNECTED;
	
	char* pEnd;
	string res = call_cmd(METHOD_STATUS);
	
	if (res.compare("playing") == 0)
		(*status) = BLUETOOTH_STATUS_PLAYING;
	else
		(*status) = BLUETOOTH_STATUS_PAUSED;
	
	return BLUETOOTH_RESULT_OK;
}

bluetooth_result BluetoothPlayer::next() {
	if (is_connected() == BLUETOOTH_RESULT_NOT_CONNECTED)
		return BLUETOOTH_RESULT_NOT_CONNECTED;
	
	call_cmd(METHOD_NEXT);
	return BLUETOOTH_RESULT_OK;
}
bluetooth_result BluetoothPlayer::play() {
	if (is_connected() == BLUETOOTH_RESULT_NOT_CONNECTED)
		return BLUETOOTH_RESULT_NOT_CONNECTED;
	
	call_cmd(METHOD_PLAY);
	return BLUETOOTH_RESULT_OK;
}
bluetooth_result BluetoothPlayer::pause() {
	if (is_connected() == BLUETOOTH_RESULT_NOT_CONNECTED)
		return BLUETOOTH_RESULT_NOT_CONNECTED;
	
	call_cmd(METHOD_PAUSE);
	return BLUETOOTH_RESULT_OK;
}

void BluetoothPlayer::register_listener(BLUETOOTH_UPDATED* listener) {
	m_update_listeners.push_back(listener);
}
void BluetoothPlayer::unregister_listener(BLUETOOTH_UPDATED* listener) {
	for (vector<BLUETOOTH_UPDATED*>::iterator it = m_update_listeners.begin(); it != m_update_listeners.end(); it++) {
		if (*it == listener) {
			m_update_listeners.erase(it);
			return;
		}
	}
}

void BluetoothPlayer::update() {
	bool connected = false;
	char* album = "";
	char* artist = "";
	char* title = "";
	int duration = 0;
	int pos = 0;
	bluetooth_status status = BLUETOOTH_STATUS_PAUSED;
	
	connected = is_connected() == BLUETOOTH_RESULT_OK;
	get_track_info(&album, &artist, &title, &duration);
	get_pos(&pos);
	get_status(&status);
	
	bool update = false;
	if (connected != m_update_connected)
		update = true;
	if (string(album).compare(string(m_update_album)) != 0)
		update = true;
	if (string(artist).compare(string(m_update_artist)) != 0)
		update = true;
	if (string(title).compare(string(m_update_title)) != 0)
		update = true;
	if (duration != m_update_duration)
		update = true;
	if (pos != m_update_pos)
		update = true;
	if (status != m_update_status)
		update = true;
	
	m_update_connected = connected;
	m_update_album = album;
	m_update_artist = artist;
	m_update_title = title;
	m_update_duration = duration;
	m_update_pos = pos;
	m_update_status = status;
	
	if (update) {
		for (int i = 0; i < m_update_listeners.size(); i++) {
			(*m_update_listeners.at(i))();
		}
	}
}

BluetoothPlayer::BluetoothPlayer() {												 
	putenv("DBUS_SESSION_BUS_ADDRESS=unix:path=/run/dbus/system_bus_socket");
	m_device_mac = string("");
	
	m_update_connected = false;
	m_update_album = "";
	m_update_artist = "";
	m_update_title = "";
	m_update_duration = 0;
	m_update_pos = 0;
	m_update_status = BLUETOOTH_STATUS_PAUSED;
}

string BluetoothPlayer::call_cmd(string method) {
	string cmd = string("qdbus --system org.bluez /org/bluez/hci0/dev_") + m_device_mac + string("/player0 ") + method;
	return call_system_read_out(cmd);
}
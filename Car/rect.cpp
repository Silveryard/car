#include "rect.h"

#include <unistd.h>

Rect::Rect(int x, int y, int width, int height, int r, int g, int b, int a)
{
	m_x = x;
	m_y = y;

	m_w = width;
	m_h = height;

	m_r = r;
	m_g = g;
	m_b = b;
	m_a = a;

	m_cmd_set = NULL;
	m_cmd_count = 0;

	size_changed = true;
	pos_changed = true;
	col_changed = true;


	update();
}

void Rect::set_pos(int x, int y)
{
	m_x = x;
	m_y = y;

	pos_changed = true;

	update();
}
void Rect::set_size(int width, int height)
{
	m_w = width;
	m_h = height;

	size_changed = true;

	update();
}
void Rect::set_color(int r, int g, int b, int a)
{
	m_r = r;
	m_g = g;
	m_b = b;
	m_a = a;

	col_changed = true;

	update();
}

int Rect::get_x()
{
	return m_x;
}
int Rect::get_y()
{
	return m_y;
}
int Rect::get_width()
{
	return m_w;
}
int Rect::get_height()
{
	return m_h;
}
void Rect::get_color(int* r, int* g, int* b, int* a)
{
	*r = m_r;
	*g = m_g;
	*b = m_b;
	*a = m_a;
}

void Rect::get_commands(drawable_cmd** commands, int* count)
{
	*commands = m_cmd_set;
	*count = m_cmd_count;
}

void Rect::update()
{
	type = "Rect";
	if (size_changed)
	{	
		if (m_cmd_set != NULL)
		{
			for (int i = 0; i < m_cmd_count; i++)
				delete m_cmd_set[i].data;
			delete m_cmd_set;
		}

		m_cmd_count = 0;
		m_cmd_set = new drawable_cmd[m_h * m_w];
	
		for (int x = 0; x < m_w; x++)
		{
			for (int y = 0; y < m_h; y++)
			{
				int i = x + y * m_w;
				m_cmd_set[i].type = m_a == 255 ? DRAWABLE_CMD_PIXEL : DRAWABLE_CMD_PIXEL_ALPHA;
				m_cmd_set[i].x = m_x + x;
				m_cmd_set[i].y = m_y + y;
				m_cmd_set[i].width = 1;
				m_cmd_set[i].height = 1;
				m_cmd_set[i].data = new int[1];
				m_cmd_set[i].data[0] = Screen::get_instance()->get_color(m_r, m_g, m_b, m_a);
			}
		}
	
		m_cmd_count = m_h*m_w;
	}
	else
	{
		if (pos_changed)
		{
			for (int x = 0; x < m_w; x++)
			{
				for (int y = 0; y < m_h; y++)
				{
					int i = x + y * m_w;
					m_cmd_set[i].x = m_x + x;
					m_cmd_set[i].y = m_y + y;
				}
			}
		}

		if (col_changed)
		{
			int c = Screen::get_instance()->get_color(m_r, m_g, m_b, m_a);

			for (int x = 0; x < m_w; x++)
			{
				for (int y = 0; y < m_h; y++)
				{
					int i = x + y * m_w;
					m_cmd_set[i].data[0] = c;
				}
			}
		}
	} 

	size_changed = false;
	pos_changed =  false;
	col_changed =  false;

	/*
	for (int i = 0; i < m_h; i++)
	{
		m_cmd_set[i].type = DRAWABLE_CMD_ROW;
		m_cmd_set[i].x = m_x;
		m_cmd_set[i].y = i + m_y;
		m_cmd_set[i].width = m_w;
		m_cmd_set[i].height = 1;
		m_cmd_set[i].data = new int[m_w];

		for (int j = 0; j < m_w; j++)
			m_cmd_set[i].data[j] = Screen::get_instance()->get_color(m_r, m_g, m_b, m_a);
	}*/
}
#include "framebuffer.h"

#include <unistd.h>
#include <fcntl.h>
#include <iostream>
#include <stdio.h>
#include <string.h>
#include <linux/ioctl.h>
#include <linux/fb.h>
#include <linux/input.h>
#include <sys/mman.h>
#include <sys/kd.h>

#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"

fb_fix_screeninfo Framebuffer::s_fix;
fb_var_screeninfo Framebuffer::s_var;

Framebuffer::Framebuffer()
{
	char* fbdevice = "/dev/fb0";
	
	m_fb = open(fbdevice, O_RDWR);
	if (m_fb == -1)
	{
		perror("open fbdevice");
		return;
	}
	
	if (ioctl(m_fb, FBIOGET_FSCREENINFO, &s_fix) < 0)
	{
		perror("ioctl FBIOGET_FSCREENINFO");
		close(m_fb);
		return;
	}
	
	if (ioctl(m_fb, FBIOGET_VSCREENINFO, &s_var) < 0)
	{
		perror("ioctl FBIOGET_VSCREENINFO");
		close(m_fb);
		return;
	}
	
	s_var.grayscale = 0;
	s_var.bits_per_pixel = 32;
	s_var.yoffset = 0;
	s_var.red.offset = 16;
	s_var.red.length = 8;
	s_var.green.offset = 8;
	s_var.green.length = 8;
	s_var.blue.offset = 0;
	s_var.blue.length = 8;
	s_var.transp.offset = 24;
	s_var.transp.length = 8;
	ioctl(m_fb, FBIOPUT_VSCREENINFO, &s_var);
	ioctl(m_fb, FBIOGET_VSCREENINFO, &s_var);
	ioctl(m_fb, FBIOPAN_DISPLAY, &s_var);


	printf("original %dx%d, %dbpp\n", s_var.xres, s_var.yres, s_var.bits_per_pixel);
	printf("Framebuffer %s%s%s resolution;\n", KYEL, fbdevice, KWHT);
	printf("%dx%d, %d bpp\n\n\n", s_var.xres, s_var.yres, s_var.bits_per_pixel);
	
	m_screensize = s_var.yres_virtual * s_fix.line_length;
	m_fbp = (char*)mmap(0,
		m_screensize,
		PROT_READ | PROT_WRITE,
		MAP_SHARED,
		m_fb,
		(off_t)0);
	if ((int)m_fbp == -1)
	{
		printf("Failed to mmap.\n");
	}
	m_bbp = new char[m_screensize];
	//swap_buffers();
	//swap_buffers();
	
	m_res_x = s_var.xres;
	m_res_y = s_var.yres;
	
	m_max_pos = (m_res_x + s_var.xoffset) * (s_var.bits_per_pixel / 8) + (m_res_y + s_var.yoffset) * s_fix.line_length;
	m_min_pos = (0 + s_var.xoffset) * (s_var.bits_per_pixel / 8) + (0 + s_var.yoffset) * s_fix.line_length;
	
	if (ioctl(m_fb, KDSETMODE, KD_GRAPHICS) < 0)
		printf("Setting KD_GRAPHICS failed\n");
}
Framebuffer::~Framebuffer()
{
	ioctl(m_fb, KDSETMODE, KD_TEXT);

	close(m_fb);
}

int Framebuffer::get_res_x()
{
	return m_res_x;
}
int Framebuffer::get_res_y()
{
	return m_res_y;
}

int Framebuffer::create_color(int r, int g, int b, int a) {
	return (r << s_var.red.offset) | (g << s_var.green.offset) | (b << s_var.blue.offset) | (a << s_var.transp.offset);
}
void Framebuffer::get_rgba(int c, int* r, int* g, int* b, int* a) {
	(*r) = (c >> s_var.red.offset) & 0xFF;
	(*g) = (c >> s_var.green.offset) & 0xFF;
	(*b) = (c >> s_var.blue.offset) & 0xFF;
	(*a) = (c >> s_var.transp.offset) & 0xFF;
}

void Framebuffer::set_pixel(int x, int y, int c)
{
	long location = (x + s_var.xoffset) * (s_var.bits_per_pixel / 8) + (y + s_var.yoffset) * s_fix.line_length;
	
	if (location < m_min_pos || location > m_max_pos)
	{
		printf("out of bounds");
		return;
	}

	*((int*)(m_bbp + location)) = c;

}

void Framebuffer::set_pixel_alpha(int x, int y, int c)
{
	long location = (x + s_var.xoffset) * (s_var.bits_per_pixel / 8) + (y + s_var.yoffset) * s_fix.line_length;
	
	if (location < m_min_pos || location > m_max_pos)
	{
		printf("out of bounds");
		return;
	}
	
	int* pP = ((int*)(m_bbp + location));
	int pL = *pP;
	int n;

	asm(
		//Load Vals
		"MOV r0, %[pL];\n\t" 
		"MOV r1, %[c];\n\t" 

		//Create RGBA
		"MOV r2, #255;\n\t"

		"AND r6, r2, r0;\n\t"
		"AND r5, r2, r0, ASR #8;\n\t"
		"AND r4, r2, r0, ASR #16;\n\t"
		"AND r3, r2, r0, ASR #24;\n\t"

		"AND r10, r2, r1;\n\t"
		"AND r9, r2, r1, ASR #8;\n\t"
		"AND r8, r2, r1, ASR #16;\n\t"
		"AND r7, r2, r1, ASR #24;\n\t"

		//Calc new RGBA
		"ADD r0, r6, #1;\n\t"
		"RSB r1, r6, #256;\n\t"

		"MUL r2, r1, r8;\n\t"
		"MUL r4, r0, r4;\n\t"
		"ADD r4, r4, r2;\n\t"
		"MOV r4, r4, LSR #8;\n\t"

		"MUL r2, r1, r9;\n\t"
		"MUL r5, r0, r5;\n\t"
		"ADD r5, r5, r2;\n\t"
		"MOV r5, r5, LSR #8;\n\t"

		"MUL r2, r1, r10;\n\t"
		"MUL r6, r0, r6;\n\t"
		"ADD r6, r6, r2;\n\t"
		"MOV r6, r6, LSR #8;\n\t"

		"SUB r2, r0, #1;\n\t"
		"RSB r3, r3, #255;\n\t"
		"MUL r3, r3, r7;\n\t"
		"MOV r3, r3, LSR #8;\n\t"
		"ADD r3, r3, r2;\n\t"

		//Create Color Int
		"MOV r0, r6;\n\t"
		"ORR r0, r5, LSL #8;\n\t"
		"ORR r0, r4, LSL #16;\n\t"
		"ORR r0, r3, LSL #24;\n\t"

		//"MOV r1, %[pP];\n\t"
		//"STR r1, [r0, #-44];\n\t"

		"MOV %[n], r0;\n\t"
		: [n] "=r" (n) 
		: [pL] "r" (pL),
		[c] "r" (c)//, [pP] "g" (pP) 
		: "r0",
		"r1",
		"r2",
		"r3",
		"r4",
		"r5",
		"r6",
		"r7",
		"r8",
		"r9",
		"r10");

	*pP = n;

	/*
	int c_r, c_g, c_b, c_a;
	get_rgba(c, &c_r, &c_g, &c_b, &c_a);
	int n_c;

	if (c_a == 255) {
		n_c = c;
	}
	else {
		//Alpha Blending
		int o_c = *((int*)(m_bbp + location));
		int o_r, o_g, o_b, o_a;
		int n_r, n_g, n_b, n_a;
		get_rgba(o_c, &o_r, &o_g, &o_b, &o_a);
		
		int al = c_a + 1;
		int ial = 256 - c_a;

		n_r = (al * c_r + ial * o_r) >> 8;
		n_g = (al * c_g + ial * o_g) >> 8;
		n_b = (al * c_b + ial * o_b) >> 8;
		n_a = (al - 1) + (1 - ((float)al / 255)) * o_a;

		n_c = create_color(n_r, n_g, n_b, n_a);
	}

	
	*((int*)(m_bbp + location)) = n_c;*/
}

void Framebuffer::set_row(int x, int y, int len, int* data) {
	long location = (x + s_var.xoffset) * (s_var.bits_per_pixel / 8) + (y + s_var.yoffset) * s_fix.line_length;
	
	memcpy((int*)(m_bbp + location), data, sizeof(int) * len);
}
void Framebuffer::set_rect_alpha(int x, int y, int width, int height, int* data)
{
	/* 
		As in set_pixel_alpha. Plus:
		R13: Cur Address
		R12: Buffer Address

		Buffer:
		base_x
		line_len
		x
		y
		width
		height
		data ptr
		base offset
	*/

	int* buffer = new int[8];
	int* of = (int*)m_bbp;
	int ll = (int)s_fix.line_length;
	int n;

	asm(
		"MOV r12, %[buffer];\n\t"
		"STR %[base_x], [r12, #0];\n\t"
		"STR %[line_len], [r12, #4];\n\t"
		"STR %[base_x], [r12, #8];\n\t"
		"STR %[base_y], [r12, #12];\n\t"
		"STR %[width], [r12, #16];\n\t"
		"STR %[height], [r12, #20];\n\t"
		"STR %[data], [r12, #24];\n\t"
		"STR %[base_off], [r12, #28];\n\t"
		::[base_x] "r" (x),
		[base_y] "r" (y),
		[line_len] "r" (ll),
		[width] "r" (width),
		[height] "r" (height),
		[buffer] "r" (buffer),
		[base_off] "r" (of),
		[data] "r" (data)
		: "r12");

	asm(
	"MOV r0, r0;\n\t"
	:::"r1",
		"r2",
		"r3",
		"r4",
		"r5",
		"r6",
		"r7",
		"r8",
		"r8",
		"r10",
		"r12",
		"r13");

	/*
		Block 1
		Load X, Y, W, H

		X++

		if X >= W
		    jump to block 2
		else
		    jump to block 3
	*/


abc:
	int p = 0;
//	asm(
//		//calc new pos
//		"LDR r0, [r12, #16];\n\t"
//		"LDR r1, [r12, #20];\n\t"
//		"LDR r2, [r12, #8];\n\t"
//		"LDR r3, [r12, #12];\n\t"
//
//		"ADD r2, #1;\n\t"
//
//		"CMP r2, r0;\n\t"
//		"BGE block2;\n\t"
//		"B block3;\n\t"
//	);

	/* 
		Block 2 (x + 1 >= w)

		Y++

		if Y >= H
			jump to end
		else
			jump to block 4
	*/

block2:
//	asm(
//		"ADD r3, #1;\n\t"
//
//		"CMP r3, r1;\n\t"
//		"BGE end;\n\t"
//		"B block4;\n\t"
//	);

	/*
		Block 3 (x + 1 < w)
		Add x + 1
		jump to block 5
	*/

block3:

	/* 
		Block 4 (y + 1 < h && x + 1 >= w)
		Load BaseX
		x = BaseX
		Add y + 1
		jump to block 5
	*/

block4:

	/*
		Block 5
		Store X, Y
		Load LineLen
		Load BaseAddr
		Load DataAddr
		CurPos = x + y * LineLen;
		CurAddr = BaseAddr + CurPos
	
		Load CurAddr

		Create Color
		Store Color
		jump to block1
	*/

block5:


		/*
		//Load Vals
		"MOV r0, %[pL];\n\t" 
		"MOV r1, %[c];\n\t" 

		//Create RGBA
		"MOV r2, #255;\n\t"

		"AND r6, r2, r0;\n\t"
		"AND r5, r2, r0, ASR #8;\n\t"
		"AND r4, r2, r0, ASR #16;\n\t"
		"AND r3, r2, r0, ASR #24;\n\t"

		"AND r10, r2, r1;\n\t"
		"AND r9, r2, r1, ASR #8;\n\t"
		"AND r8, r2, r1, ASR #16;\n\t"
		"AND r7, r2, r1, ASR #24;\n\t"

		//Calc new RGBA
		"ADD r0, r6, #1;\n\t"
		"RSB r1, r6, #256;\n\t"

		"MUL r2, r1, r8;\n\t"
		"MUL r4, r0, r4;\n\t"
		"ADD r4, r4, r2;\n\t"
		"MOV r4, r4, LSR #8;\n\t"

		"MUL r2, r1, r9;\n\t"
		"MUL r5, r0, r5;\n\t"
		"ADD r5, r5, r2;\n\t"
		"MOV r5, r5, LSR #8;\n\t"

		"MUL r2, r1, r10;\n\t"
		"MUL r6, r0, r6;\n\t"
		"ADD r6, r6, r2;\n\t"
		"MOV r6, r6, LSR #8;\n\t"

		"SUB r2, r0, #1;\n\t"
		"RSB r3, r3, #255;\n\t"
		"MUL r3, r3, r7;\n\t"
		"MOV r3, r3, LSR #8;\n\t"
		"ADD r3, r3, r2;\n\t"

		//Create Color Int
		"MOV r0, r6;\n\t"
		"ORR r0, r5, LSL #8;\n\t"
		"ORR r0, r4, LSL #16;\n\t"
		"ORR r0, r3, LSL #24;\n\t"

		//"MOV r1, %[pP];\n\t"
		//"STR r1, [r0, #-44];\n\t"
		"MOV %[n], r0;\n\t"
		: [n] "=r" (n) 
		: 
		: "r0",
		"r1",
		"r2",
		"r3",
		"r4",
		"r5",
		"r6",
		"r7",
		"r8",
		"r9",
		"r10",
		"r11",
		"r12");*/

endb:
	int i = 0;
}
void Framebuffer::fill(int* data) {
	memcpy((int*)(m_bbp + m_min_pos), data, sizeof(int) * m_res_x * m_res_y);
}
void Framebuffer::swap_buffers()
{	
	/*if (s_var.yoffset == 0)
		s_var.yoffset = s_var.yres;
	else
		s_var.yoffset = 0;
	
	ioctl(m_fb, FBIOPUT_VSCREENINFO, &s_var);
	
	m_max_pos = (m_res_x + s_var.xoffset) * (s_var.bits_per_pixel / 8) + (m_res_y + s_var.yoffset) * s_fix.line_length;
	m_min_pos = (0 + s_var.xoffset) * (s_var.bits_per_pixel / 8) + (0 + s_var.yoffset) * s_fix.line_length;
	
	char* tmp;
	tmp = m_fbp;
	m_fbp = m_bbp;
	m_bbp = tmp;*/

	memcpy(m_fbp, m_bbp, m_screensize);
}
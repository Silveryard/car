#ifndef _CAR_LINE_H_
#define _CAR_LINE_H_

#include "screen.h"

class Line : public Drawable
{
private:
	int m_x, m_y, m_len, m_r, m_g, m_b, m_a;
	bool m_hor;

	drawable_cmd* m_cmd_set;
	int m_cmd_count;

public:
	Line(int x, int y, int len, bool hor, int r, int g, int b, int a);
	
	void update_pos(int x, int y);
	void update_len(int len);
	void update_orientation(bool hor);
	void update_color(int r, int g, int b, int a);
	
	int get_x();
	int get_y();
	int get_len();

	virtual void get_commands(drawable_cmd** commands, int* count);
	
private:
	void update();
};

#endif

#include "section.h"

#include "bluetooth_player.h"
#include "globals.h"

using namespace std;

BluetoothSection* b_instance;

void bluetooth_update() {
	b_instance->update_sec();
}

BluetoothSection::BluetoothSection(int id, UPDATE_METHOD* method, SET_METHOD* setter, Font* serif_12) : Section(id, method, setter) {
	b_instance = this;
	m_drawables = vector<Drawable*>();
	m_serif_12 = serif_12;
	m_str_not_connected = new CString(m_serif_12, 235, 200, "No device connected");
		
	m_str_not_connected->set_pos(400 - (m_str_not_connected->get_width() / 2.0f), 240 - (m_str_not_connected->get_height() / 2.0f));
		
	m_bt_alb = new CString(m_serif_12, 10, 10, "");
	m_bt_art = new CString(m_serif_12, 10, 10, "");
	m_bt_tit = new CString(m_serif_12, 10, 10, "");	
	BluetoothPlayer::get_instance()->register_listener(&bluetooth_update);
	
	m_track_hor = new Line(BUTTON_WIDTH + 30, 350, 800 - (2 * (BUTTON_WIDTH + 30)), true, 255, 255, 255, 255);
	m_track_ver = new Line(BUTTON_WIDTH + 30, 330, 40, false, 255, 255, 255, 255);
	
	m_img_play = Image::load(690, BUTTON_HEIGHT * 0 + 25, "/home/pi/car/system/images/play.rif");
	m_img_pause = Image::load(690, BUTTON_HEIGHT * 0 + 25, "/home/pi/car/system/images/pause.rif");
	m_img_next = Image::load(690, BUTTON_HEIGHT * 3 + 25, "/home/pi/car/system/images/next.rif");
}

void BluetoothSection::update_sec() {
	char *alb = "";
	char *art = "";
	char *tit = "";
	int dur;
	
	BluetoothPlayer::get_instance()->get_track_info(&alb, &art, &tit, &dur);
	m_bt_alb->set_string(alb);
	m_bt_art->set_string(art);
	m_bt_tit->set_string(tit);
																		  
	m_bt_tit->set_pos(400 - (m_bt_tit->get_width() / 2.0f), 100);
	m_bt_alb->set_pos(400 - (m_bt_alb->get_width() / 2.0f), 150);
	m_bt_art->set_pos(400 - (m_bt_art->get_width() / 2.0f), 200);
	
	if (dur != 0) {
		int pos;
		BluetoothPlayer::get_instance()->get_pos(&pos);
		
		int min_x = m_track_hor->get_x();
		int max_x = m_track_hor->get_x() + m_track_hor->get_len();
		
		int x = min_x + ((max_x - min_x) * (pos / (float)dur));
		m_track_ver->update_pos(x, m_track_ver->get_y());	
	}
	
	bluetooth_status stat;
	BluetoothPlayer::get_instance()->get_status(&stat);
	bool n_p = stat == BLUETOOTH_STATUS_PAUSED; 
	bool u = n_p != m_paused;
	m_paused = n_p;
	
	if (u)
		this->update();
}

vector<Drawable*>* BluetoothSection::get_drawables() {
	m_drawables.clear();
	
	if (BluetoothPlayer::get_instance()->is_connected() == BLUETOOTH_RESULT_NOT_CONNECTED) {
		m_drawables.push_back(m_str_not_connected);
	}
	else {
		m_drawables.push_back(m_bt_alb);
		m_drawables.push_back(m_bt_art);
		m_drawables.push_back(m_bt_tit);
		m_drawables.push_back(m_track_hor);
		m_drawables.push_back(m_track_ver);
		
		if (m_paused)
			m_drawables.push_back(m_img_play);
		else
			m_drawables.push_back(m_img_pause);
		
		m_drawables.push_back(m_img_next);
	}
	
	return &m_drawables;
}
void BluetoothSection::update_input(section_input input) {
	if (input.input1) {
		if (m_paused)
			BluetoothPlayer::get_instance()->play();
		else
			BluetoothPlayer::get_instance()->pause();
	}	
	
	if (input.input4) {
		BluetoothPlayer::get_instance()->next();
	}
}
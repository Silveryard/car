#ifndef _CAR_RECT_H_
#define _CAR_RECT_H_

#include "screen.h"

class Rect : public Drawable
{
private:
	int m_x, m_y;
	int m_w, m_h;
	int m_r, m_g, m_b, m_a;

	bool size_changed;
	bool pos_changed;
	bool col_changed;

	drawable_cmd* m_cmd_set;
	int m_cmd_count;

public:
	Rect(int x, int y, int width, int height, int r, int g, int b, int a);

	void set_pos(int x, int y);
	void set_size(int width, int height);
	void set_color(int r, int g, int b, int a);

	int get_x();
	int get_y();
	int get_width();
	int get_height();
	void get_color(int* r, int* g, int* b, int* a);

	virtual void get_commands(drawable_cmd** commands, int* count);

private:
	void update();
};

#endif
#ifndef _CAR_USBMANAGER_H_
#define _CAR_USBMANAGER_H_

#include <vector>
#include <map>

struct usb_drive {
	char* device_path;
	char* mount_path;
	char* serial;
	char* name;
};

class UsbManager {
private:
	static UsbManager* s_instance;

public:
	typedef void(USB_DRIVE_MOUNTED)(usb_drive* drive);
	typedef void(USB_DRIVE_UNMOUNTED)(usb_drive* drive);
	
	static UsbManager* get_instance();

private:
	std::vector<usb_drive*> m_mounted_drives;
	std::vector<USB_DRIVE_MOUNTED*> m_mounted_listeners;
	std::vector<USB_DRIVE_UNMOUNTED*> m_unmounted_listeners;
	
public:
	UsbManager();
	~UsbManager();
	
	void register_mounted_listener(USB_DRIVE_MOUNTED* listener);
	void unregister_mounted_listener(USB_DRIVE_MOUNTED* listener);
	
	void register_unmounted_listener(USB_DRIVE_UNMOUNTED* listener);
	void unregister_unmounted_listener(USB_DRIVE_UNMOUNTED* listener);
	
	std::vector<usb_drive*>* get_mounted_drives();
	
	void update();
	
private:
	void get_mounted_device_paths(std::vector<char*>* devices);
	char* get_mount_path(char* device_path);
	char* get_serial(char* device_path);
	char* get_name(char* device_path);
};

#endif
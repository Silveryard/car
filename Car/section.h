#ifndef _CAR_SECTION_H_
#define _CAR_SECTION_H_

#include <vector>
#include <map>

#include "framebuffer.h"
#include "image.h"
#include "CString.h"
#include "list.h"
#include "line.h"

#include "music_lib.h"
#include <fmod.hpp>

struct section_input {
	bool input1, input2, input3, input4;
};

class Section {
public:
	typedef void(UPDATE_METHOD)(int id);
	typedef void(SET_METHOD)(int id);
	
private:
	int m_id;
	UPDATE_METHOD* m_update_method;
	SET_METHOD* m_set_method;
	
public:
	Section(int id, UPDATE_METHOD* updater, SET_METHOD* setter);
	
	virtual std::vector<Drawable*>* get_drawables() = 0;
	virtual void update_input(section_input input) = 0;
	
	void update();
	void set_section(int id);
};

class HomeSection : public Section {
private:
	std::vector<Drawable*> m_drawables;
	Image* m_skoda_logo;
	CString* m_bt_str;
	
public:
	HomeSection(int width, int height, int id, UPDATE_METHOD* method, SET_METHOD* setter, Font* serif_16);
	
	virtual std::vector<Drawable*>* get_drawables();
	virtual void update_input(section_input input);
};

class RadioSection : public Section {
private:
	struct sender
	{
		CString* name;
		char* tune;
	};

	const int INDEX_SENDER_GONG = 0;
	const int INDEX_SENDER_GALAXY = 1;
	const int INDEX_SENDER_SWR3 = 2;

	bool m_muted;

	std::vector<Drawable*> m_drawables;
	std::vector<sender> m_senders;
	List* m_list;

	Image* m_img_up;
	Image* m_img_down;
	Image* m_img_play;
	Image* m_img_pause;
	Image* m_img_select;

public:
	RadioSection(int id, UPDATE_METHOD* method, SET_METHOD* setter, Font* serif_12);
	
	virtual std::vector<Drawable*>* get_drawables();
	virtual void update_input(section_input input);

private:
	void set_sender(sender s);
	void set_volume(int vol);
	void set_mute(bool mute);
};

class MusicSection : public Section {
private:
	enum MODE {
		MODE_HOME,
		MODE_ALL_TITLES,
		MODE_ARTISTS,
		MODE_ALBUMS,
		MODE_ALBUM,
		MODE_CUR_TRACK
	};
	
	//Music Data
	FMOD::System* m_music_system;
	Song* m_cur_song;
	FMOD::Sound* m_cur_sound;
	FMOD::Channel* m_channel;
	std::vector<Song*> m_autoplay_list;
	bool m_autoplay;
	
	//Constants
	Font* m_serif_12;
	int m_button_width;
	int m_button_height;
	
	//Selection
	List* m_list;
	Artist* m_selected_artist;
	Album* m_selected_album;
	MODE m_cur_mode;
	
	//Visuals
	std::vector<Drawable*> m_drawables;
	
	Image* btn_down;
	Image* btn_up;
	Image* btn_back;
	Image* btn_select;
	Image* btn_pause;
	Image* btn_play;
	Image* btn_next;
	
	std::vector<CString*> m_home_elements;
	std::map<Artist*, CString*> m_artist_names;
	std::map<Album*, CString*> m_album_names;
	std::map<Song*, CString*> m_song_names;
	
	CString* m_cur_alb;
	CString* m_cur_art;
	CString* m_cur_tit;
	Line* m_cur_song_hor;
	Line* m_cur_song_ver;
	
	
public:
	MusicSection(int id, UPDATE_METHOD* method, SET_METHOD* setter, int button_width, int button_height, Font* serif_12);
	
	virtual std::vector<Drawable*>* get_drawables();
	virtual void update_input(section_input input);
	
	void update_music_system();
	void update_music_lib(bool trigger = true);
	
private:
	void set_mode(MODE mode, bool trigger = true);
	int get_string_x();
	int get_string_y(int index);
	
	void next_song();
};

class SettingsSection : public Section {
public:
	typedef void(HANDLER)();
private:
	const int INDEX_HOME_VOLUME = 0;
	const int INDEX_HOME_SOUND = 1;
	const int INDEX_HOME_EXIT = 2;
	
	const int INDEX_MUSIC_AUTOPLAY = 0;
	const int INDEX_MUSIC_SHUFFLE = 1;
	
	const int INDEX_VOLUME_UP = 0;
	const int INDEX_VOLUME_STATUS = 1;
	const int INDEX_VOLUME_DOWN = 2;
	const int VOLUME_STEP = 2;
	const int VOLUME_MIN = 0;
	const int VOLUME_MAX = 100;
private:
	enum MODE {
		MODE_HOME,
		MODE_SOUND,
		MODE_VOLUME
	};
	
	MODE m_cur_mode;
	
	std::vector<Drawable*> m_drawables;
	HANDLER* m_exit_handler;
	List* m_list;
	
	int m_base_string_x;
	Font* m_serif_12;
	
	std::vector<Image*> m_buttons;
	std::vector<CString*> m_home_elements;
	std::vector<CString*> m_music_elements;
	std::vector<CString*> m_volume_elements;
	
public:
	SettingsSection(int id, UPDATE_METHOD* method, SET_METHOD* setter, HANDLER* exit_handler, Font* serif_12, int button_width, int button_height);
	
	virtual std::vector<Drawable*>* get_drawables();
	virtual void update_input(section_input input);
	
private:
	void set_mode(MODE mode, bool trigger = true);

	void set_volume();
};

class BluetoothSection : public Section {
private:
	bool m_paused;
	std::vector<Drawable*> m_drawables;
	CString* m_str_not_connected;
	CString* m_bt_alb;
	CString* m_bt_art;
	CString* m_bt_tit;
	Font* m_serif_12;
	
	Line* m_track_hor;
	Line* m_track_ver;
	
	Image* m_img_play;
	Image* m_img_pause;
	Image* m_img_next;
	
public:
	BluetoothSection(int id, UPDATE_METHOD* method, SET_METHOD* setter, Font* serif_16);
	
	void update_sec();
	
	virtual std::vector<Drawable*>* get_drawables();
	virtual void update_input(section_input input);
};

#endif
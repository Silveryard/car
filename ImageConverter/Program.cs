﻿using System;
using System.IO;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageConverter {
    class Program {
        static void Main(string[] args) {
            using (Bitmap b = new Bitmap(Image.FromFile(args[0]))) {
                byte[] data = new byte[b.Width * b.Height * 4 + 8];

                byte[] width = BitConverter.GetBytes(b.Width);
                byte[] height = BitConverter.GetBytes(b.Height);

                Reverse(width);
                Reverse(height);

                for (int i = 0; i < 4; i++)
                    data[i] = width[i];

                for (int i = 0; i < 4; i++)
                    data[i + 4] = height[i];

                for(int x = 0; x < b.Width; x++) {
                    for(int y = 0; y < b.Height; y++) {
                        int index = (x * b.Height + y) * 4 + 8;
                        Color c = b.GetPixel(x, y);

                        data[index + 0] = c.R;
                        data[index + 1] = c.G;
                        data[index + 2] = c.B;
                        data[index + 3] = c.A;
                    }
                }

                File.WriteAllBytes(args[1], data);
            }
        }

        static void Reverse(byte[] data) {
            byte[] buffer = new byte[data.Length];

            for(int i = 0; i < data.Length; i++)
                buffer[data.Length - 1 - i] = data[i];

            for (int i = 0; i < data.Length; i++)
                data[i] = buffer[i];
        }
    }
}
